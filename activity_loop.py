"""
name: activity_loop.py
This script generates periodic redfish activity for a designated user account.
It is useful for testing BMC credential management.
"""
#
# Copyright 2018-2020 Nokia, Inc. All rights reserved.
#

from __future__ import print_function
import argparse
import logging
import time
import airframetools.redfish_api as redfish

SKIP_ME = -2

def activity_loop(cmd_line):
    """
    """
    try:
        redfish.open_session(cmd_line.ipadd, username=cmd_line.uname, password=cmd_line.passw)
    except RuntimeError as rt_err:
        logging.error('(activity_loop) open_session: %s',rt_err)
    else:
        loops = range(0, int(cmd_line.loop_cnt))
        for the_loop in loops:
            loop_start = time.time()
            logging.info('(activity_loop) starting request %s', the_loop+1)
            try:
                version_information = redfish.get_bmc_version()
            except RuntimeError as rt_err:
                logging.error('(activity_loop) failure returning version information: %s', rt_err)
                break;
            else:
                logging.info('(activity_loop) request response: %s', version_information)
                loop_end = time.time()
                loop_runtime = loop_end-loop_start
                logging.info('(activity_loop) request elapsed time = %s', loop_runtime)
                logging.info('(activity_loop) going to sleep for %s seconds', cmd_line.cadence)

            time.sleep(float(cmd_line.cadence))

        redfish.terminate_current_session()


def main():
    """
    """
    starttime = time.time()
    logging.basicConfig(level=logging.INFO)

    parser = argparse.ArgumentParser(description='Nokia (c)2018 AirFrame(R) information tool')
    parser.add_argument('ipadd', help='ip address')
    parser.add_argument('uname', help='username')
    parser.add_argument('passw', help='password')
    parser.add_argument('cadence', help='query cadence (seconds)')
    parser.add_argument('loop_cnt', help='loop counter')

    args = parser.parse_args()
    activity_loop(args)
    endtime = time.time()
    runtime = endtime-starttime
    logging.info('(main) execution time: %s', runtime)


if __name__ == '__main__':
    main()
