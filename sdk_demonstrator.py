"""

    Name: sdk_demonstrator.py
    Purpose: This module provides some example scripts for the AirFrame
    Redfish SDK.
    The examples may need to be modified to work with your server.
    Copyright 2018 - 2020 Nokia, Inc All right reserved.

"""
#
#  Copyright 2018 - 2020 Nokia, Inc All right reserved.
#
#  top-level examples script for Nokia AirFrame provisioning SDK
#
from __future__ import print_function
import json
import logging
import airframetools.redfish_api as redfish
import airframetools.legacy_api as legapi

REDFISH_API = 'redfish'
LEGACY_API = 'legacy'

class BMCLogin(object):
    """
     private object to collect information necessary to contact the BMC
    """
    def __init__(self, ip_addr, uname='admin', passw='secret'):
        self.ip_addr = ip_addr
        self.uname = uname
        self.passw = passw

class userinfo(object):
    """
    object to carry user information
    """
    def __init__(self, id, uname, passw, confirm_password):
        self.id = id 
        self.uname = uname 
        self.passw = passw
        self.confirm_password = confirm_password

def firmware_version_example(api_choice, my_bmc):
    """
    """
    if api_choice == LEGACY_API:
        try:
            legacy_session = legapi.open_session(host=my_bmc.ip_addr, username=my_bmc.uname, password=my_bmc.passw)
        except RuntimeError as rt_err:
            logging.error('(firmware_version_example) error opening session with legacy api: %s', rt_err)
        else:
            logging.info('(firmware_version_example) opened legacy BMC session')
            try:
                response = legapi.get_firmware_version(legacy_session)
            except RuntimeError as rt_err:
                logging.error(rt_err)
            else:
                print(json.dumps(response))

            legapi.close_session(legacy_session)
            logging.info('closed legacy BMC session')


def network_examples(my_bmc):
    """
    exercise network functions
    """
    redfish.open_session(my_bmc.ip_addr, my_bmc.uname, my_bmc.passw)
    logging.info('redfish session opened')
    try:
        bmc_mac_addr = redfish.get_BMC_MAC_address()
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('BMC MAC address: %s', bmc_mac_addr)

    try:
        bmc_ipv4 = redfish.get_BMC_IPv4_address()
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('BMC IPv4 address %s', bmc_ipv4)

    try:
        bmc_ipv6 = redfish.get_BMC_IPv6_address()
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('BMC IPv6 address: %s', bmc_ipv6)

    try:
        nic_info = redfish.list_all_blade_network_interface_information()
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('Printing all NIC information:')
        #print(nic_info)  # why not use logging here as well?
        logging.info(nic_info)
    redfish.terminate_current_session()
    logging.info('redfish session terminated')

def legacy_get_network_info(my_bmc):
    """
    The Legacy BMC API provides slightly different access to network
    interfaces.
    """
    try:
        legacy_session = legapi.open_session(host=my_bmc.ip_addr, username=my_bmc.uname, password=my_bmc.passw)
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('opened legacy BMC session')
        try:
            network_response = legapi.get_network_settings(legacy_session)
        except RuntimeError as rt_err:
            logging.error(rt_err)
        else:
            logging.info('Get Network settings')
            network_list = network_response.content
            #logging.info(json.dumps(network_list, sort_keys=True))
            logging.info(network_list)

        legapi.close_session(legacy_session)
        logging.info('closed legacy BMC session')

def legacy_set_IPv6_example(my_bmc):
    """
    example for setting the IPv6 address using the legacy API
    """
    try:
        legacy_session = legapi.open_session(host=my_bmc.ip_addr, username=my_bmc.uname, password=my_bmc.passw)
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        try:
            old_network_settings = legapi.get_network_settings(legacy_session)
            print('========= old network settings ==============')
            print(old_network_settings.content)
            old_net = json.loads(old_network_settings.content)
            #ipv6_config = legapi.default_ipv6_config(old_net[0])
            ipv6_config = old_net[0]
            ipv6_config['ipv6_address'] = '3ffe:501:ffff:102:222:12ff:fe22:1111'
            ipv6_config['ipv6_address'] = '2100:0:0:0:0:0:0:4'
            print('========== ipv6 configuration ==============')
            print(ipv6_config)

        except AttributeError as aterr:
            logging.info(aterr)

        try:
            set_response = legapi.set_bmc_ipv6(legacy_session, ipv6_config)
        except RuntimeError as rt_err:
            logging.error(rt_err)
            print(' ************* SET failed ***************')
        logging.info('SET response: %s', set_response.content)
        legapi.close_session(legacy_session)
        logging.info('legacy BMC session closed')

def processor_examples(my_bmc):
    """
       processor query examples
    """
    redfish.open_session(my_bmc.ip_addr, my_bmc.uname, my_bmc.passw)
    logging.info('redfish session opened')
    try:
        processor_count = redfish.get_processor_count()
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('processor_count = %s', processor_count)

    # we'll just look at processor 1 for remaining examples
    try:
        processor_name = redfish.get_processor_name(1)
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('processor 1 name: %s', processor_name)

    try:
        processor_brand = redfish.get_processor_brand(1)
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('processor 1 brand: %s', processor_brand)

    try:
        processor_version = redfish.get_processor_version(1)
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('processor 1 version: %s', processor_version)

    # now, print all information
    try:
        processor_info = redfish.list_all_processor_information()
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('Printing all processor information:')
        print(processor_info)

    redfish.terminate_current_session()
    logging.info('redfish session terminated')


def memory_examples(my_bmc):
    """
       memory query examples
    """
    redfish.open_session(my_bmc.ip_addr, my_bmc.uname, my_bmc.passw)
    logging.info('redfish session opened')

    try:
        memory_count = redfish.get_memory_count()
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('memory count = %s', memory_count)

    try:
        memory_size = redfish.get_memory_size(1)
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('Memory 1 size = %s', memory_size)

    try:
        memory_information = redfish.list_all_memory_information()
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('Printing all memory information:')
        print(memory_information)

    redfish.terminate_current_session()
    logging.info('redfish session terminated')

def power_supply_examples(my_bmc):
    """
       power supply query examples
    """
    redfish.open_session(my_bmc.ip_addr, my_bmc.uname, my_bmc.passw)
    logging.info('redfish session opened')

    try:
        power_supply_count = redfish.get_power_supply_count()
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('power supply count = %s', power_supply_count)

    power_supply_name = 'PSU0'
    try:
        power_supply_sensor_info = redfish.get_power_supply_sensor_information(power_supply_name)
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('Printing power supply information for %s:', power_supply_name)
        print(power_supply_sensor_info)

    try:
        all_power_supply_information = redfish.list_all_power_supply_sensor_information()
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('Printing all power supply information:')
        print(all_power_supply_information)

    redfish.terminate_current_session()
    logging.info('redfish session terminated')

def fan_examples(my_bmc):
    """
       fan query examples
    """
    redfish.open_session(my_bmc.ip_addr, my_bmc.uname, my_bmc.passw)
    logging.info('redfish session opened')

    try:
        fan_count = redfish.get_fan_sensor_count()
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('fan sensor count = %s', fan_count)

    sensor_name = 'Fan_SYS0_0'
    try:
        single_fan_information = redfish.get_fan_sensor_information(sensor_name)
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('Printing fan sensor information for %s', sensor_name)
        print(single_fan_information)

    try:
        all_fan_sensor_information = redfish.list_all_fan_sensor_information()
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('Printing all fan sensor information:')
        print(all_fan_sensor_information)

    redfish.terminate_current_session()
    logging.info('redfish session terminated')

def disk_examples(my_bmc):
    """
       disk query examples
    """
    redfish.open_session(my_bmc.ip_addr, my_bmc.uname, my_bmc.passw)
    logging.info('redfish session opened')

    try:
        disk_count = redfish.get_chassis_disk_count()
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('disk count = %s', disk_count)

    drive_id = 1
    try:
        drive_information = redfish.get_disk_information(drive_id)
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('Printing information for disk #%s', drive_id)
        print(drive_information)

    try:
        all_disk_information = redfish.list_all_disk_information()
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('Printing all disk information:')
        print(all_disk_information)

    redfish.terminate_current_session()
    logging.info('redfish session terminated')

def system_examples(my_bmc):
    """
       system query examples
    """
    redfish.open_session(my_bmc.ip_addr, my_bmc.uname, my_bmc.passw)
    logging.info('redfish session opened')

    try:
        all_system_information = redfish.list_all_system_information()
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('Printing system information')
        print(all_system_information)

    redfish.terminate_current_session()
    logging.info('redfish session terminated')

def legacy_user_account_examples(my_bmc):
    """
       user account query examples
    """
    try:
        legacy_session = legapi.open_session(host=my_bmc.ip_addr, username=my_bmc.uname, password=my_bmc.passw)
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('opened legacy BMC session')
        try:
            user_list = legapi.get_users_info(legacy_session)
        except RuntimeError as rt_err:
            logging.error(rt_err)
        else:
            print(json.dumps(user_list, sort_keys = True))
        legapi.close_session(legacy_session)
        logging.info('closed legacy BMC session')

def media_data_example():
    """
      This example prepares the dictionary used for setting media options.
    """
    media_data = {
        'id': 1,
        'cd_remote_domain_name': 'nokia.com',  # optional, FQDN for CD server
        'cd_remote_server_address': '10.130.10.20', # IP address for server where CD remote media images are stored
        'cd_remote_share_type': 'nfs',  # specifies  Share Type of the remote media server: NFS or Samba(CIFS)
        'cd_remote_source_path': '/media/cd_drive',  # source path to remote images
        'cd_remote_user_name': 'user1', # if share type is Samba, user credentials to authenticate to server
        'cd_remote_password': 'goodpassword',
        'fd_remote_domain_name': '',
        'fd_remote_password': '',
        'fd_remote_server_address': '',
        'fd_remote_share_type': '',
        'fd_remote_source_path': '',
        'fd_remote_user_name': '',
        'hd_remote_domain_name': '',
        'hd_remote_password': '',
        'hd_remote_server_address': '',
        'hd_remote_share_type': '',
        'hd_remote_source_path': '',
        'hd_remote_user_name': '',
        'local_media_support': '1',    # booleans, 0 = false, 1 = true
        'mount_cd': '1',
        'mount_fd': '0',
        'mount_hd': '0',
        'remote_media_support': '1',
        'same_settings': '1'   # 1 = share settings between CD/Floppy/Harddisk
    }
    return media_data


def media_examples(my_bmc):
    """
       bmc media examples
    """
    try:
        legacy_session = legapi.open_session(host=my_bmc.ip_addr, username=my_bmc.uname, password=my_bmc.passw)
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('legacy BMC session opened')
        try:
            media_information = legapi.get_media_general(legacy_session)
        except RuntimeError as rt_err:
            logging.error(rt_err)
        else:
            logging.info('Printing media information retrieved from BMC')
            print(media_information)

        media_data = media_data_example()
        try:
            set_result = legapi.set_media_general(legacy_session, media_data)
        except RuntimeError as rt_err:
            logging.error(rt_err)
        else:
            logging.info('Printing response from set_media_general')
            print(set_result)

        legapi.close_session(legacy_session)
        logging.info('legacy BMC session closed')

def get_smtp_example(legacy_session):
    """
       example for retrieving the BMC SMTP configuration
    """
    try:
        smtp_attributes = legapi.get_smtp_attributes(legacy_session)
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('Printing SMTP attributes')
        print(json.dumps(smtp_attributes))

def example_smtp_configuration():
    """
      This example prepares the dictionary used in setting the SMTP configuration.
    """
    smtp_configuration = {
        'id': 1,                              # smtp settings identifier
        'channel_interface': 'bond0',         # Ethernet interface name
        'email_id' : 'example_bmc_email_address@nokia.com',
        'primary_smtp_enable': 1,             # boolean: 1 to enable/0 to disable
        'primary_server_name': 'mailrelay.int.nokia.com',
        'primary_smtp_port': 25,
        'primary_smtp_secure_port': 465,
        'primary_server_ip': '10.130.128.21',
        'primary_smtp_authentication': 0,     # boolean: 1 to enable/0 to disable
        'primary_username': 'admin',
        'primary_ssltls_enable': 0,           # boolean: 1 to enable/0 to disable SMTP SSL/TLS protocol
        'primary_starttls_enable': 0,         # boolean: 1 to enable/0 to disable SMTP STARTTLS protocol
        'primary_password': 'goodpassword',
        'ca_info1': '',                       # string, must be in .pem format
        'cert_info1': '',                     # string, must be in .pem format
        'key_info1': '',                      # string, must be in .pem format
        'secondary_server_ip': '',
        'secondary_smtp_enable': 0,           # boolean: 1 to enable/0 to disable
        'secondary_server_name': '',
        'secondary_smtp_port': 25,
        'secondary_smtp_secure_port': 465,
        'secondary_smtp_authentication': 0,   # boolean: 1 to enable/0 to disable
        'secondary_username': '',
        'secondary_ssltls_enable': 0,         # boolean: 1 to enable/0 to disable SMTP SSL/TLS protocol
        'secondary_starttls_enable': 0,       # boolean: 1 to enable/0 to disable SMTP STARTTLS protocol
        'secondary_password': 'betterpassword',
        'ca_info2': '',
        'cert_info2': '',
        'key_info2': ''
    }
    return smtp_configuration

def set_smtp_example(legacy_session):
    """
        example for retrieving the BMC SMTP configuration
    """
    smtp_configuration = example_smtp_configuration()
    try:
        set_result = legapi.set_smtp_attributes(legacy_session, smtp_configuration)
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('printing result of set request')
        print(json.dumps(set_result))

def smtp_examples(my_bmc):
    """
      SMTP examples
    """
    try:
        legacy_session = legapi.open_session(host=my_bmc.ip_addr, username=my_bmc.uname, password=my_bmc.passw)
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('legacy BMC session opened')
        get_smtp_example(legacy_session)
        set_smtp_example(legacy_session)

        legapi.close_session(legacy_session)
        logging.info('legacy BMC session closed')

def bmc_config_example(my_bmc):
    """
        Show how to get preservation parameters using the SDK
    """
    redfish.open_session(my_bmc.ip_addr, my_bmc.uname, my_bmc.passw)
    logging.info('redfish session opened')
    try:
        configuration = redfish.get_bmc_configuration_preservation()
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('BMC Preservation Configuration Before %s', configuration)
    try:
        redfish.set_bmc_configuration_preservation(Authentication=True, IPMI=True, KVM=False, Network=True,
                                                   SEL=True, SNMP=True, SSH=True)
        configuration = redfish.get_bmc_configuration_preservation()

    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('BMC Preservation Configuration After %s', configuration)

    redfish.terminate_current_session()
    logging.info('redfish session terminated')
    return

def get_bios_example(my_bmc, attribute):
    """
        Show how to get BIOS parameters using the SDK
        In this example attribute maps to a given BIOS parameter
    """
    redfish.open_session(my_bmc.ip_addr, my_bmc.uname, my_bmc.passw)
    logging.info('redfish session opened')
    try:
        configuration = redfish.get_bios_attribute(attribute)
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('BIOS Configuration of %s is %s', attribute, configuration)

    redfish.terminate_current_session()
    logging.info('redfish session terminated')
    return

def bmc_reset_example(my_bmc):
    """
       BMC Reset examples
    """
    redfish.open_session(my_bmc.ip_addr, my_bmc.uname, my_bmc.passw)
    logging.info('redfish session opened')

    try:
        redfish.reset_BMC()
    except RuntimeError as rt_err:
        logging.error(rt_err)

    redfish.terminate_current_session()
    logging.info('redfish session terminated')

def create_redfish_acct_example(my_bmc,account_username, new_password, roleid):
    """
        Create a new Redfish user
    """
    redfish.open_session(my_bmc.ip_addr, my_bmc.uname, my_bmc.passw)
    logging.info('redfish session opened')

    try:
        #set_user_result = redfish.set_user_account_password(account_username,new_password)
        create_response = redfish.create_user_account(account_username,new_password, roleid)
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('Create New User')
        print(json.dumps(create_response))

    redfish.terminate_current_session()
    logging.info('redfish session terminated')

def create_legacy_account_example(my_bmc):
    """
        Exercise the legacy API Function for Creating accounts
    """
    # add_new_user(bmc_session, new_userinfo)
    userinfo.uname = 'legvdms'
    userinfo.passw = 'cmb10.admin'
    userinfo.confirm_password = userinfo.passw
    try:
        legacy_session = legapi.open_session(host=my_bmc.ip_addr, username=my_bmc.uname, password=my_bmc.passw)
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('Legacy Session Opened')
        try:
            user_response = legapi.add_new_user(legacy_session, userinfo)
        except AttributeError as aterr:
            logging.info(aterr)
        else:
            logging.info('Legacy Create User')
            logging.info(user_response)

        legapi.close_session(legacy_session)
        logging.info('legacy BMC session closed')
        return 

def redfish_user_account_examples(my_bmc):
    """
       Examples using Redfish of user accounts
       Grabs all user information
    """
    redfish.open_session(my_bmc.ip_addr, my_bmc.uname, my_bmc.passw)
    logging.info('redfish session opened')

    try:
        all_system_information = redfish.list_user_accounts()
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('Printing user information')
        print(json.dumps(all_system_information, sort_keys=True))

    redfish.terminate_current_session()
    logging.info('redfish session terminated')

def intrusion_examples(my_bmc):
    """
        Examples for intrusion detection
    """
#    redfish.open_session(my_bmc.ip_addr, my_bmc.uname, my_bmc.passw)
#    logging.info('redfish session opened')
#    try:
#        intrusion_status = redfish.get_intrusion_status()
#    except RuntimeError as rt_err:
#        logging.error(rt_err)
#    else:
#        logging.info('(intrusion_examples) intrusion status = %s', intrusion_status)
#    redfish.terminate_current_session()
#    logging.info('redfish session terminated')

    try:
        legacy_session = legapi.open_session(host=my_bmc.ip_addr, username=my_bmc.uname, password=my_bmc.passw)
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        logging.info('(intrusion_examples) legacy BMC session opened')
        LAN_info = {
                'name':'waldo',
                'subject':'intrusion example',
                'message':'the server has been intruded upon'
                }
        event_filter_spec = {}
        legapi.set_LAN_destination(legacy_session, LAN_info)
        legapi.enable_event_filter(legacy_session, event_filter_spec)

        legapi.close_session(legacy_session)
        logging.info('(intrusion_examples) legacy BMC session closed')



# ----------------------------------------------------------

def main():
    """
       list of demonstrator examples
    """
    logging.basicConfig(level=logging.DEBUG)
    bmc_example = BMCLogin('10.18.36.214', passw='cmb9.admin')

    do_bios_settings           = False
    do_bmc_config_get          = False
    do_leg_bmc_network_example = False
    do_bmc_reset_example       = False # This will restart the BMC
    do_net_addresses = False
    do_legacy_set_IPv6 = False
    do_proc_status = False
    do_mem_queries = False
    do_power_status = False
    do_fan_status = False
    query_system = False
    do_disk_queries = False
    do_user_accounts = False
    do_create_legacy_account = False
    do_media_examples        = False
    do_smtp_examples         = False
    do_intrusion_examples    = True

    if do_bios_settings is True:
        attribute = "IIOS295"
        attribute = "IPMI104"
        get_bios_example(bmc_example, attribute)

    if do_bmc_config_get is True:
        bmc_config_example(bmc_example)
   
    if do_leg_bmc_network_example is True:
        legacy_get_network_info(bmc_example)

    if do_bmc_reset_example is True:
        bmc_reset_example(bmc_example)

    if do_net_addresses:
        network_examples(bmc_example)

    if do_legacy_set_IPv6 is True:
        legacy_set_IPv6_example(bmc_example)

    if do_proc_status is True:
        processor_examples(bmc_example)

    if do_mem_queries is True:
        memory_examples(bmc_example)

    if do_power_status is True:
        power_supply_examples(bmc_example)

    if do_fan_status is True:
        fan_examples(bmc_example)

    if do_disk_queries is True:
        disk_examples(bmc_example)

    if query_system is True:
        system_examples(bmc_example)

    if do_user_accounts is True:
        legacy_user_account_examples(bmc_example)

    if do_create_legacy_account:
        create_legacy_account_example(bmc_example)

    if do_media_examples is True:
        media_examples(bmc_example)

    if do_smtp_examples is True:
        smtp_examples(bmc_example)

    if do_intrusion_examples:
        intrusion_examples(bmc_example)
#--------------
main()
