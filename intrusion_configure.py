from __future__ import print_function
import json
import logging
import airframetools.redfish_api as redfish
import airframetools.legacy_api as legapi

class BMCLogin(object):
    """
     private object to collect information necessary to contact the BMC
    """
    def __init__(self, ip_addr, uname='admin', passw='secret'):
        self.ip_addr = ip_addr
        self.uname = uname
        self.passw = passw

def main():
    logging.basicConfig(level=logging.DEBUG)
    bmc_example = BMCLogin('10.18.36.214', passw='cmb9.admin')
    legacy_session = legapi.open_session(host=bmc_example.ip_addr, username=bmc_example.uname, password=bmc_example.passw)

    smtp_template = legapi.get_smtp_attributes(legacy_session)
    print(json.dumps(smtp_template))


    legapi.close_session(legacy_session)

if __name__ == "__main__":
    main()
