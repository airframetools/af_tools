"""
Name: RedfishSession.py
Session Handling Module
"""
#
#  Copyright 2018 - 2020 Nokia, Inc All rights reserved.
#  This module contains the Redfish session object
#

import json
import logging
import requests

class RedfishSession(object):
    """
    Class object for Redfish Session
    """
    def __init__(self, host, username, password):
        self.host = str(host)
        self.username = str(username)
        self.password = str(password)
        self.authentication = requests.auth.HTTPBasicAuth(username, password)

        requests.packages.urllib3.disable_warnings(requests.packages.urllib3.exceptions.InsecureRequestWarning)

        self.__session = requests.Session()
        self.__session.headers['Content-Type'] = 'application/json'
        status_code, headers, content = self.execute_post_request('/redfish/v1/SessionService/Sessions')

        try:
            id_number = content['Id']
        except KeyError:
            raise RuntimeError('Cannot create redfish session!')
        else:
            self.id_number = id_number

    def _format_response(self, response):
        status_code = response.status_code
        headers = dict(response.headers)
        try:
            content = response.json()
        except ValueError:
            content = response.text
        return status_code, headers, content

    def _log_response(self, status_code, headers, content):
        logging.debug(json.dumps({'status_code': status_code, 'headers': headers, 'content': content}, indent=2))

    def execute_delete_request(self, command, data=None):
        """
          wrapper for  DELETE requests
        """
        url = 'https://{}{}'.format(self.host, command)
        data = json.dumps(data if data is not None else {'UserName': self.username, 'Password': self.password})

        response = self.__session.delete(url, data=data, auth=self.authentication, verify=False)
        status_code, headers, content = self._format_response(response)
        self._log_response(status_code, headers, content)
        return status_code, headers, content

    def execute_get_request(self, command):
        """
          wrapper for GET requests
        """
        url = 'https://{}{}'.format(self.host, command)

        response = self.__session.get(url, auth=self.authentication, verify=False)
        status_code, headers, content = self._format_response(response)
        self._log_response(status_code, headers, content)
        return status_code, headers, content

    def execute_patch_request(self, command, data=None):
        """
          wrapper for PATCH requests
        """
        url = 'https://{}{}'.format(self.host, command)
        data = json.dumps(data if data is not None else {'UserName': self.username, 'Password': self.password})

        response = self.__session.patch(url, data=data, auth=self.authentication, verify=False)
        status_code, headers, content = self._format_response(response)
        self._log_response(status_code, headers, content)
        return status_code, headers, content

    def execute_post_request(self, command, data=None):
        """
          wrapper for POST requests
        """
        url = 'https://{}{}'.format(self.host, command)
        data = json.dumps(data if data is not None else {'UserName': self.username, 'Password': self.password})

        response = self.__session.post(url, data=data, auth=self.authentication, verify=False)
        status_code, headers, content = self._format_response(response)
        self._log_response(status_code, headers, content)
        return status_code, headers, content

    def execute_put_request(self, command, data=None):
        """
          wrapper for PUT requests
        """
        url = 'https://{}{}'.format(self.host, command)
        data = json.dumps(data if data is not None else {'UserName': self.username, 'Password': self.password})

        response = self.__session.put(url, data=data, auth=self.authentication, verify=False)
        status_code, headers, content = self._format_response(response)
        self._log_response(status_code, headers, content)
        return status_code, headers, content

    def terminate(self):
        """
          terminates the session
        """
        self.execute_delete_request('/redfish/v1/SessionService/Sessions/{}'.format(self.id_number))
