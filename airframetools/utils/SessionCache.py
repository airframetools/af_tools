#  Copyright 2018 - 2020 Nokia, Inc All rights reserved.

"""
Name: SessionCache.py
Session  Module to cache session objects.  This prevents
uncessarily reopening sessions
"""
from airframetools.utils.RedfishSession import RedfishSession


class ConnectionCache(object):
    """
    Cache Object
    """

    class NoConnection(object):
        """
        No connection object Inheriting from ConnectionCache
        """
        def __getattr__(self, attribute):
            if attribute.startswith('__') and attribute.endswith('__'):
                raise AttributeError
            raise RuntimeError('No connection found.')

        def __nonzero__(self):
            return False

    def __init__(self):
        self._no_current = ConnectionCache.NoConnection()
        self.empty_cache()

    def __iter__(self):
        return iter(self._connections)

    def __len__(self):
        return len(self._connections)

    def __nonzero__(self):
        return self.current is not self._no_current

    @property
    def active_connections(self):
        """
        Name: active_connections
        """
        active_connections = []
        for host, connections in self._connections.items():
            active_connections.extend(connections)
        return active_connections

    def open_connection(self, connection):
        """
        Name: open_connection
        """
        if connection.host not in self._connections.keys():
            self._connections.update({connection.host: []})
        self._connections[connection.host].append(connection)
        self.current = connection

    def switch(self, host, id_number):
        """
        Name: switch
        get connection
        """
        self.current = self.get_connection(host, id_number)
        return self.current

    def get_connection(self, host, id_number):
        """
        Name: get_connection
        """
        try:
            connection = next(item for item in
                              filter(lambda c: c.id_number == id_number, self._connections.get(host, [])))
        except StopIteration:
            raise RuntimeError('Non-existing connection at "{}" with id "{}".'.format(host, id_number))
        else:
            return connection

    def empty_cache(self):
        """
        Name: empty_cache
        """
        self.current = self._no_current
        self._connections = {}

    def terminate(self):
        """
        Name: terminate
        """
        if self.current:
            self.current.terminate()
            self._connections[self.current.host].remove(self.current)
            self.current = self._no_current

    def terminate_all(self):
        """
        Name: terminate_all
        """
        for connection in self.active_connections:
            connection.terminate()
        self.empty_cache()


class SessionCache(object):
    """
    Name SessionCache
    """
    @classmethod
    def get_cache(cls):
        """
        Name: get_cache
        """
        try:
            cls._cache
        except AttributeError:
            cls._cache = ConnectionCache()
        return cls._cache

    @classmethod
    def get_current_session(cls):
        """
        Name: get_current_session
        """
        return cls.get_cache().current

    @classmethod
    def get_current_session_host(cls):
        """
        Name:
        """
        return cls.get_current_session().host

    @classmethod
    def get_current_session_id(cls):
        """
        Name:
        """
        return cls.get_current_session().id_number

    @classmethod
    def open_session(cls, host, username, password):
        """
        Name:
        """
        cls.get_cache().open_connection(RedfishSession(host, username, password))
        return cls.get_current_session_id()

    @classmethod
    def switch_session(cls, host, session_id):
        """
        Name:
        """
        cls.get_cache().switch(host, session_id)

    @classmethod
    def terminate_current_session(cls):
        """
        Name:
        """
        cls.get_cache().terminate()

    @classmethod
    def terminate_all_sessions(cls):
        """
        Name:
        """
        cls.get_cache().terminate_all()

    @classmethod
    def execute_get_request(cls, command):
        """
        Name:
        """
        return cls.get_current_session().execute_get_request(command)

    @classmethod
    def execute_patch_request(cls, command, data=None):
        """
        Name:
        """
        return cls.get_current_session().execute_patch_request(command, data=data)

    @classmethod
    def execute_post_request(cls, command, data=None):
        """
        Name:
        """
        return cls.get_current_session().execute_post_request(command, data=data)

    @classmethod
    def execute_put_request(cls, command, data=None):
        """
        Name:
        """
        return cls.get_current_session().execute_put_request(command, data=data)

    @classmethod
    def execute_delete_request(cls, command, data=None):
        """
        """
        return cls.get_current_session().execute_delete_request(command, data=data)
