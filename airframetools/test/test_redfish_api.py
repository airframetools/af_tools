import unittest
from airframetools.redfish_api import (
    list_all_fan_sensor_information, list_all_fan_sensor_reading, list_all_fan_sensor_status,
    get_fan_sensor_count, get_fan_sensor_information, get_fan_sensor_reading, get_fan_sensor_status,

    list_all_temperature_sensor_information, list_all_temperature_sensor_reading, list_all_temperature_sensor_status,
    get_temperature_sensor_information, get_temperature_sensor_reading, get_temperature_sensor_status,

    list_all_voltage_sensor_information, list_all_voltage_sensor_reading, list_all_voltage_sensor_status,
    get_voltage_sensor_information, get_voltage_sensor_reading, get_voltage_sensor_status
)
from mock import patch


class FanSensorTestCase(unittest.TestCase):
    """
    Tests for fan sensor related redfish commands.
    """

    RESPONSE = {
        'Fans': [
            {'Name': 'Fan 1', 'Reading': 'Reading 1', 'Status': 'Status 1'},
            {'Name': 'Fan 2', 'Reading': 'Reading 2', 'Status': 'Status 2'}
        ]
    }

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_list_all_fan_sensor_information_failure(self, mock_get):
        """Listing all fan sensor information should raise exception."""
        mock_get.return_value = (200, {}, {})
        with self.assertRaisesRegexp(RuntimeError, 'Cannot list fan sensors!'):
            list_all_fan_sensor_information()

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_list_all_fan_sensor_information_success(self, mock_get):
        """Listing all fan sensor information should pass."""
        mock_get.return_value = (200, {}, FanSensorTestCase.RESPONSE)
        self.assertEqual(FanSensorTestCase.RESPONSE['Fans'], list_all_fan_sensor_information())

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_get_fan_sensor_count_failure(self, mock_get):
        """Getting fan sensor count should raise exception."""
        mock_get.return_value = (200, {}, {})
        with self.assertRaisesRegexp(RuntimeError, 'Cannot get fan sensor count!'):
            get_fan_sensor_count()

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_get_fan_sensor_count_success(self, mock_get):
        """Getting fan sensor count should pass."""
        mock_get.return_value = (200, {}, FanSensorTestCase.RESPONSE)
        self.assertEqual(2, get_fan_sensor_count())

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_list_all_fan_sensor_reading_failure(self, mock_get):
        """Listing all fan sensor reading should raise exception."""
        mock_get.return_value = (200, {}, {})
        with self.assertRaisesRegexp(RuntimeError, 'Cannot list all fan sensor readings!'):
            list_all_fan_sensor_reading()

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_list_all_fan_sensor_reading_success(self, mock_get):
        """Listing all fan sensor reading should pass."""
        mock_get.return_value = (200, {}, FanSensorTestCase.RESPONSE)
        self.assertEqual(
            [{'Name': 'Fan 1', 'Reading': 'Reading 1'}, {'Name': 'Fan 2', 'Reading': 'Reading 2'}],
            list_all_fan_sensor_reading()
        )

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_list_all_fan_sensor_status_failure(self, mock_get):
        """Listing all fan sensor status should raise exception."""
        mock_get.return_value = (200, {}, {})
        with self.assertRaisesRegexp(RuntimeError, 'Cannot list all fan sensor status!'):
            list_all_fan_sensor_status()

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_list_all_fan_sensor_status_success(self, mock_get):
        """Listing all fan sensor status should pass."""
        mock_get.return_value = (200, {}, FanSensorTestCase.RESPONSE)
        self.assertEqual(
            [{'Name': 'Fan 1', 'Status': 'Status 1'}, {'Name': 'Fan 2', 'Status': 'Status 2'}],
            list_all_fan_sensor_status()
        )

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_get_fan_sensor_information_failure(self, mock_get):
        """Getting fan sensor information should raise exception."""
        mock_get.return_value = (200, {}, {})
        with self.assertRaisesRegexp(RuntimeError, 'Cannot get fan sensor!'):
            get_fan_sensor_information('Fan 3')

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_get_fan_sensor_information_success(self, mock_get):
        """Getting fan sensor information should pass."""
        mock_get.return_value = (200, {}, FanSensorTestCase.RESPONSE)
        self.assertEqual(
            {'Name': 'Fan 2', 'Reading': 'Reading 2', 'Status': 'Status 2'},
            get_fan_sensor_information('Fan 2')
        )

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_get_fan_sensor_reading_failure(self, mock_get):
        """Getting fan sensor reading should raise exception."""
        mock_get.return_value = (200, {}, {})
        with self.assertRaisesRegexp(RuntimeError, 'Cannot get fan sensor reading!'):
            get_fan_sensor_reading('Fan 3')

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_get_fan_sensor_reading_success(self, mock_get):
        """Getting fan sensor reading should pass."""
        mock_get.return_value = (200, {}, FanSensorTestCase.RESPONSE)
        self.assertEqual(
            {'Reading': 'Reading 1'},
            get_fan_sensor_reading('Fan 1')
        )

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_get_fan_sensor_status_failure(self, mock_get):
        """Getting fan sensor status should raise exception."""
        mock_get.return_value = (200, {}, {})
        with self.assertRaisesRegexp(RuntimeError, 'Cannot get fan sensor status!'):
            get_fan_sensor_status('Fan 3')

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_get_fan_sensor_status_success(self, mock_get):
        """Getting fan sensor status should pass."""
        mock_get.return_value = (200, {}, FanSensorTestCase.RESPONSE)
        self.assertEqual(
            {'Status': 'Status 1'},
            get_fan_sensor_status('Fan 1')
        )


class TemperatureSensorTestCase(unittest.TestCase):
    """
    Tests for temperature sensor related redfish commands.
    """

    RESPONSE = {
        'Temperatures': [
            {'Name': 'Temp 1', 'ReadingCelsius': 'ReadingCelsius 1', 'Status': 'Status 1'},
            {'Name': 'Temp 2', 'ReadingCelsius': 'ReadingCelsius 2', 'Status': 'Status 2'}
        ]
    }

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_list_all_temperature_sensor_information_failure(self, mock_get):
        """Listing all temperature sensor information should raise exception."""
        mock_get.return_value = (200, {}, {})
        with self.assertRaisesRegexp(RuntimeError, 'Cannot list temperature sensors!'):
            list_all_temperature_sensor_information()

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_list_all_temperature_sensor_information_success(self, mock_get):
        """Listing all temperature sensor information should pass."""
        mock_get.return_value = (200, {}, TemperatureSensorTestCase.RESPONSE)
        self.assertEqual(TemperatureSensorTestCase.RESPONSE['Temperatures'], list_all_temperature_sensor_information())

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_list_all_temperature_sensor_reading_failure(self, mock_get):
        """Listing all temperature sensor reading should raise exception."""
        mock_get.return_value = (200, {}, {})
        with self.assertRaisesRegexp(RuntimeError, 'Cannot list all temperature sensor readings!'):
            list_all_temperature_sensor_reading()

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_list_all_temperature_sensor_reading_success(self, mock_get):
        """Listing all temperature sensor reading should pass."""
        mock_get.return_value = (200, {}, TemperatureSensorTestCase.RESPONSE)
        self.assertEqual(
            [{'Name': 'Temp 1', 'ReadingCelsius': 'ReadingCelsius 1'}, {'Name': 'Temp 2', 'ReadingCelsius': 'ReadingCelsius 2'}],
            list_all_temperature_sensor_reading()
        )

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_list_all_temperature_sensor_status_failure(self, mock_get):
        """Listing all temperature sensor status should raise exception."""
        mock_get.return_value = (200, {}, {})
        with self.assertRaisesRegexp(RuntimeError, 'Cannot list all temperature sensor status!'):
            list_all_temperature_sensor_status()

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_list_all_temperature_sensor_status_success(self, mock_get):
        """Listing all temperature sensor status should pass."""
        mock_get.return_value = (200, {}, TemperatureSensorTestCase.RESPONSE)
        self.assertEqual(
            [{'Name': 'Temp 1', 'Status': 'Status 1'}, {'Name': 'Temp 2', 'Status': 'Status 2'}],
            list_all_temperature_sensor_status()
        )

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_get_temperature_sensor_information_failure(self, mock_get):
        """Getting temperature sensor information should raise exception."""
        mock_get.return_value = (200, {}, {})
        with self.assertRaisesRegexp(RuntimeError, 'Cannot get temperature sensor!'):
            get_temperature_sensor_information('Temp 3')

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_get_temperature_sensor_information_success(self, mock_get):
        """Getting temperature sensor information should pass."""
        mock_get.return_value = (200, {}, TemperatureSensorTestCase.RESPONSE)
        self.assertEqual(
            {'Name': 'Temp 2', 'ReadingCelsius': 'ReadingCelsius 2', 'Status': 'Status 2'},
            get_temperature_sensor_information('Temp 2')
        )

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_get_temperature_sensor_reading_failure(self, mock_get):
        """Getting temperature sensor reading should raise exception."""
        mock_get.return_value = (200, {}, {})
        with self.assertRaisesRegexp(RuntimeError, 'Cannot get temperature sensor reading!'):
            get_temperature_sensor_reading('Temp 3')

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_get_temperature_sensor_reading_success(self, mock_get):
        """Getting temperature sensor reading should pass."""
        mock_get.return_value = (200, {}, TemperatureSensorTestCase.RESPONSE)
        self.assertEqual(
            {'ReadingCelsius': 'ReadingCelsius 1'},
            get_temperature_sensor_reading('Temp 1')
        )

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_get_temperature_sensor_status_failure(self, mock_get):
        """Getting temperature sensor status should raise exception."""
        mock_get.return_value = (200, {}, {})
        with self.assertRaisesRegexp(RuntimeError, 'Cannot get temperature sensor status!'):
            get_temperature_sensor_status('Temp 3')

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_get_temperature_sensor_status_success(self, mock_get):
        """Getting temperature sensor status should pass."""
        mock_get.return_value = (200, {}, TemperatureSensorTestCase.RESPONSE)
        self.assertEqual(
            {'Status': 'Status 1'},
            get_temperature_sensor_status('Temp 1')
        )


class VoltageSensorTestCase(unittest.TestCase):
    """
    Tests for voltage sensor related redfish commands.
    """

    RESPONSE = {
        'Voltages': [
            {'Name': 'Volt 1', 'ReadingVolts': 'ReadingVolts 1', 'Status': 'Status 1'},
            {'Name': 'Volt 2', 'ReadingVolts': 'ReadingVolts 2', 'Status': 'Status 2'}
        ]
    }

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_list_all_voltage_sensor_information_failure(self, mock_get):
        """Listing all voltage sensor information should raise exception."""
        mock_get.return_value = (200, {}, {})
        with self.assertRaisesRegexp(RuntimeError, 'Cannot list voltage sensors!'):
            list_all_voltage_sensor_information()

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_list_all_voltage_sensor_information_success(self, mock_get):
        """Listing all voltage sensor information should pass."""
        mock_get.return_value = (200, {}, VoltageSensorTestCase.RESPONSE)
        self.assertEqual(VoltageSensorTestCase.RESPONSE['Voltages'], list_all_voltage_sensor_information())

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_list_all_voltage_sensor_reading_failure(self, mock_get):
        """Listing all voltage sensor reading should raise exception."""
        mock_get.return_value = (200, {}, {})
        with self.assertRaisesRegexp(RuntimeError, 'Cannot list all voltage sensor readings!'):
            list_all_voltage_sensor_reading()

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_list_all_voltage_sensor_reading_success(self, mock_get):
        """Listing all voltage sensor reading should pass."""
        mock_get.return_value = (200, {}, VoltageSensorTestCase.RESPONSE)
        self.assertEqual(
            [{'Name': 'Volt 1', 'ReadingVolts': 'ReadingVolts 1'}, {'Name': 'Volt 2', 'ReadingVolts': 'ReadingVolts 2'}],
            list_all_voltage_sensor_reading()
        )

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_list_all_voltage_sensor_status_failure(self, mock_get):
        """Listing all voltage sensor status should raise exception."""
        mock_get.return_value = (200, {}, {})
        with self.assertRaisesRegexp(RuntimeError, 'Cannot list all voltage sensor status!'):
            list_all_voltage_sensor_status()

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_list_all_voltage_sensor_status_success(self, mock_get):
        """Listing all voltage sensor status should pass."""
        mock_get.return_value = (200, {}, VoltageSensorTestCase.RESPONSE)
        self.assertEqual(
            [{'Name': 'Volt 1', 'Status': 'Status 1'}, {'Name': 'Volt 2', 'Status': 'Status 2'}],
            list_all_voltage_sensor_status()
        )

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_get_voltage_sensor_information_failure(self, mock_get):
        """Getting voltage sensor information should raise exception."""
        mock_get.return_value = (200, {}, {})
        with self.assertRaisesRegexp(RuntimeError, 'Cannot get voltage sensor!'):
            get_voltage_sensor_information('Volt 3')

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_get_voltage_sensor_information_success(self, mock_get):
        """Getting voltage sensor information should pass."""
        mock_get.return_value = (200, {}, VoltageSensorTestCase.RESPONSE)
        self.assertEqual(
            {'Name': 'Volt 2', 'ReadingVolts': 'ReadingVolts 2', 'Status': 'Status 2'},
            get_voltage_sensor_information('Volt 2')
        )

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_get_voltage_sensor_reading_failure(self, mock_get):
        """Getting voltage sensor reading should raise exception."""
        mock_get.return_value = (200, {}, {})
        with self.assertRaisesRegexp(RuntimeError, 'Cannot get voltage sensor reading!'):
            get_voltage_sensor_reading('Volt 3')

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_get_voltage_sensor_reading_success(self, mock_get):
        """Getting voltage sensor reading should pass."""
        mock_get.return_value = (200, {}, VoltageSensorTestCase.RESPONSE)
        self.assertEqual(
            {'ReadingVolts': 'ReadingVolts 1'},
            get_voltage_sensor_reading('Volt 1')
        )

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_get_voltage_sensor_status_failure(self, mock_get):
        """Getting voltage sensor status should raise exception."""
        mock_get.return_value = (200, {}, {})
        with self.assertRaisesRegexp(RuntimeError, 'Cannot get voltage sensor status!'):
            get_voltage_sensor_status('Volt 3')

    @patch('airframetools.utils.SessionCache.SessionCache.execute_get_request')
    def test_get_voltage_sensor_status_success(self, mock_get):
        """Getting voltage sensor status should pass."""
        mock_get.return_value = (200, {}, VoltageSensorTestCase.RESPONSE)
        self.assertEqual(
            {'Status': 'Status 1'},
            get_voltage_sensor_status('Volt 1')
        )


if __name__ == '__main__':
    unittest.main()
