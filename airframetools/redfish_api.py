"""
    Name: redfish_api.py
    Purpose: redfish_api is the primary OOB provisioning 
    library for the Nokia AirFrame Server

"""
#
#
#  Copyright 2018 - 2020 Nokia, Inc All rights reserved.
#  This module is an API library for native Redfish calls
#

import json
import httplib
import logging
from airframetools.utils.SessionCache import SessionCache
from version import VERSION

__version__ = VERSION


def open_session(host, username='Administrator', password='superuser'):
    """Creates a session with the host using the given credentials.

    This method returns the session id of the created session,
    which can be used to switch back to that session later.

    Example:
    session_id = open_session("127.0.0.1", username="myUserName", password="myPassword")
    """
    return SessionCache.open_session(host, username, password)


def get_current_session_host():
    """Returns the host of the current session.

    Example:
    open_session("127.0.0.1", username="myUserName", password="myPassword")
    session_host = get_current_session_host()
    """
    return SessionCache.get_current_session().host


def get_current_session_id():
    """Returns the session id of the current session.

    Example:
    open_session("127.0.0.1", username="myUserName", password="myPassword")
    session_id = get_current_session_id()
    """
    return SessionCache.get_current_session().id_number


def switch_session(host, session_id):
    """Switches between created sessions using the given session id.

    Calling the `open_session` method will always return the session id.

    Example:
    first_session_id = open_session("127.0.0.1", username="myUserName", password="myPassword")
    second_session_id = open_session("127.0.0.1", username="myUserName", password="myPassword")
    switch_session(first_session_id)
    """
    SessionCache.switch_session(host, session_id)


def terminate_current_session():
    """Terminates the current session.

    Use `terminate_all_sessions` if you want to make sure that all opened
    sessions are terminated.

    Example:
    session_id = open_session("127.0.0.1", username="myUserName", password="myPassword")
    terminate_current_session()
    """
    SessionCache.terminate_current_session()


def terminate_all_sessions():
    """Terminates all open sessions and empties the session cache.

    If multiple sessions are opened, this method should be used
    to make sure that all sessions are closed.
    It is not an error if some of the sessions have already been terminated
    by `terminate_session`.

    Example:
    first_session_id = open_session("127.0.0.1", username="myUserName", password="myPassword")
    second_session_id = open_session("127.0.0.1", username="myUserName", password="myPassword")
    terminate_all_sessions()
    """
    SessionCache.terminate_all_sessions()


def get_flash_status():
    """Returns the flash status via the current session.

    Example:
    status = get_flash_status()
    """
    status_code, headers, content = SessionCache.execute_get_request('/redfish/v1/UpdateService')
    try:
        status = content['Oem']['FlashStatus']
    except KeyError:
        raise RuntimeError('Cannot get flash status!')
    else:
        return {'FlashStatus': status}


def get_bmc_version():
    """Returns the BMC version via the current session.

    Example:
    version = get_BMC_version()
    """
    status_code, headers, content = SessionCache.execute_get_request('/redfish/v1/Systems/Self')
    try:
        version = content['Oem']['Nokia_RackScale']['FirmwareVersion']
    except KeyError:
        raise RuntimeError('Cannot get BMC version!')
    else:
        return {'FirmwareVersion': version}


def get_bmc_configuration_preservation():
    """Returns the BMC configuration preservation via the current session.

    Returned configuration:
    Authentication, IPMI, KVM, Network, SEL, SNMP, SSH

    Example:
    configuration = get_bmc_configuration_preservation()
    """
    status_code, headers, content = SessionCache.execute_get_request('/redfish/v1/UpdateService')
    try:
        configuration = content['Oem']['BMC']['PreserveConfiguration']
    except KeyError:
        raise RuntimeError('Cannot get BMC configuration preservation!')
    else:
        return configuration


def set_bmc_configuration_preservation(**configurations):
    """Sets the BMC configuration preservation via the current session.

    Accepted configurations are:
    Authentication, IPMI, KVM, Network, SEL, SNMP, SSH

    Example:
    set_BMC_configuration_preservation(IPMI=True, SNMP=False)
    """
    status_code, headers, content = SessionCache.execute_patch_request(
        '/redfish/v1/UpdateService',
        data={
            'Oem': {
                'BMC': {
                    'PreserveConfiguration': configurations
                }
            }
        }
    )
    if status_code != httplib.OK:
        raise RuntimeError('Cannot set BMC configuration preservation!')


def update_bmc_firmware(remote_image_path):
    """Updates the BMC firmware with the given binary image file via the current session.

    ATTENTION:
    Set proper BMC configuration preservation!
    Make sure that the update does not violate any of the supported upgrade paths!

    Example:
    set_bmc_configuration_preservation(Authentication=True, IPMI=True, KVM=True, Network=True,
                                      SEL=True, SNMP=True, SSH=True)
    update_bmc_firmware("http://path/to/image.ima_enc")
    """
    logging.debug('(update_bmc_firmware) remote_image_path = %s', str(remote_image_path))
    status_code, headers, content = SessionCache.execute_post_request(
        '/redfish/v1/UpdateService/Actions/Oem/UpdateService.BMCFwUpdate',
        data={
            'RemoteImagePath': str(remote_image_path),
            'FlashType': 'FULLFwUpdate'
        }
    )
    if status_code != httplib.NO_CONTENT:
        raise RuntimeError('Cannot update BMC firmware!')
    logging.debug('(update_bmc_firmware) status_code: %s', status_code)
    logging.debug('(update_bmc_firmware) content: %s', content)



def get_bios_version():
    """Returns the BIOS version via the current session.

    Example:
    version = get_bios_version()
    """
    status_code, headers, content = SessionCache.execute_get_request('/redfish/v1/Systems/Self')
    try:
        version = content['BiosVersion']
    except KeyError:
        raise RuntimeError('Cannot get BIOS version!')
    else:
        return {'BiosVersion': version}


def update_bios_firmware(remote_image_path, preserve=True):
    """Updates the BIOS firmware with the given binary image file via the current session.

    ATTENTION:
    Turn the payload off before the update!
    Set proper BIOS configuration preservation!
    Make sure that the update does not violate any of the supported upgrade paths!

    Example:
    update_bios_firmware("http://path/to/image.BIN_enc", preserve=True)
    """
    logging.debug('(update_bios_firmware) remote_image_path = %s', remote_image_path)
    status_code, headers, content = SessionCache.execute_post_request(
        '/redfish/v1/UpdateService/Actions/Oem/UpdateService.BIOSFwUpdate',
        data={
            'RemoteImagePath': str(remote_image_path),
            'PreserveBIOSNVRAMRegion': preserve
        }
    )
    if status_code != httplib.NO_CONTENT:
        raise RuntimeError('Cannot update BIOS firmware!')
    logging.debug('(update_bios_firmware) status-code = %s', status_code)
    logging.debug('(update_bios_firmware) content = %s', content)


def list_all_fan_sensor_information():
    """Lists all fan sensor information via the current session.

    Example:
    fan_sensors = list_all_fan_sensor_information()
    """
    status_code, headers, content = SessionCache.execute_get_request('/redfish/v1/Chassis/Self/Thermal')
    try:
        sensors = content['Fans']
    except KeyError:
        raise RuntimeError('Cannot list fan sensors!')
    else:
        return sensors


def get_fan_sensor_count():
    """Returns the fan sensor count via the current session.

    Example:
    fan_sensor_count = get_fan_sensor_count()
    """
    try:
        count = len(list_all_fan_sensor_information())
    except RuntimeError:
        raise RuntimeError('Cannot get fan sensor count!')
    else:
        return count


def list_all_fan_sensor_reading():
    """Lists all fan sensor reading via the current session.

    Example:
    fan_sensor_readings = list_all_fan_sensor_reading()
    """
    try:
        readings = []
        for sensor in list_all_fan_sensor_information():
            readings.append({'Name': sensor['Name'], 'Reading': sensor['Reading']})
    except (KeyError, RuntimeError):
        raise RuntimeError('Cannot list all fan sensor readings!')
    else:
        return readings


def list_all_fan_sensor_status():
    """Lists all fan sensor status via the current session.

    Example:
    fan_sensor_status = list_all_fan_sensor_status()
    """
    try:
        status = []
        for sensor in list_all_fan_sensor_information():
            status.append({'Name': sensor['Name'], 'Status': sensor['Status']})
    except (KeyError, RuntimeError):
        raise RuntimeError('Cannot list all fan sensor status!')
    else:
        return status


def get_fan_sensor_information(sensor_name):
    """Gets the fan sensor information with the given sensor name via the current session.

    Example:
    fan_sensor = get_fan_sensor_information("Fan_SYS0_0")
    """
    status_code, headers, content = SessionCache.execute_get_request('/redfish/v1/Chassis/Self/Thermal')
    try:
        sensor = next(item for item in filter(lambda f: f['Name'] == sensor_name, content['Fans']))
    except (KeyError, StopIteration):
        raise RuntimeError('Cannot get fan sensor for sensor name = {}!'.format(sensor_name))
    else:
        return sensor


def get_fan_sensor_reading(sensor_name):
    """Gets the fan sensor reading with the given sensor name via the current session.

    Example:
    fan_sensor_reading = get_fan_sensor_reading("Fan_SYS0_0")
    """
    try:
        reading = get_fan_sensor_information(sensor_name)['Reading']
    except (KeyError, RuntimeError):
        raise RuntimeError('Cannot get fan sensor reading for sensor_name = {}!'.format(sensor_name))
    else:
        return {'Reading': reading}


def get_fan_sensor_status(sensor_name):
    """Gets the fan sensor status with the given sensor name via the current session.

    Example:
    fan_sensor_status = get_fan_sensor_status("Fan_SYS0_0")
    """
    try:
        status = get_fan_sensor_information(sensor_name)['Status']
    except (KeyError, RuntimeError):
        raise RuntimeError('Cannot get fan sensor status!')
    else:
        return {'Status': status}


def list_all_temperature_sensor_information():
    """Lists all temperature sensor information via the current session.

    Example:
    temperature_sensors = list_all_temperature_sensor_information()
    """
    status_code, headers, content = SessionCache.execute_get_request('/redfish/v1/Chassis/Self/Thermal')
    try:
        sensors = content['Temperatures']
    except KeyError:
        raise RuntimeError('Cannot list temperature sensors!')
    else:
        return sensors


def list_all_temperature_sensor_reading():
    """Lists all temperature sensor reading via the current session.

    Example:
    temperature_sensor_readings = list_all_temperature_sensor_reading()
    """
    try:
        readings = []
        for sensor in list_all_temperature_sensor_information():
            readings.append({'Name': sensor['Name'], 'ReadingCelsius': sensor['ReadingCelsius']})
    except (KeyError, RuntimeError):
        raise RuntimeError('Cannot list all temperature sensor readings!')
    else:
        return readings


def list_all_temperature_sensor_status():
    """Lists all temperature sensor status via the current session.

    Example:
    temperature_sensor_status = list_all_temperature_sensor_status()
    """
    try:
        status = []
        for sensor in list_all_temperature_sensor_information():
            status.append({'Name': sensor['Name'], 'Status': sensor['Status']})
    except (KeyError, RuntimeError):
        raise RuntimeError('Cannot list all temperature sensor status!')
    else:
        return status


def get_temperature_sensor_information(sensor_name):
    """Gets the temperature sensor information with the given sensor name via the current session.

    Example:
    temperature_sensor = get_temperature_sensor_information("Temp_PSU0")
    """
    status_code, headers, content = SessionCache.execute_get_request('/redfish/v1/Chassis/Self/Thermal')
    try:
        sensor = next(item for item in filter(lambda t: t['Name'] == sensor_name, content['Temperatures']))
    except (KeyError, StopIteration):
        raise RuntimeError('Cannot get temperature sensor for sensor_name = {}!'.format(sensor_name))
    else:
        return sensor


def get_temperature_sensor_reading(sensor_name):
    """Gets the temperature sensor reading with the given sensor name via the current session.

    Example:
    temperature_sensor_reading = get_temperature_sensor_reading("Temp_PSU0")
    """
    try:
        reading = get_temperature_sensor_information(sensor_name)['ReadingCelsius']
    except (KeyError, RuntimeError):
        raise RuntimeError('Cannot get temperature sensor reading!')
    else:
        return {'ReadingCelsius': reading}


def get_temperature_sensor_status(sensor_name):
    """Gets the temperature sensor status with the given sensor name via the current session.

    Example:
    temperature_sensor_status = get_temperature_sensor_status("Temp_PSU0")
    """
    try:
        status = get_temperature_sensor_information(sensor_name)['Status']
    except (KeyError, RuntimeError):
        raise RuntimeError('Cannot get temperature sensor status!')
    else:
        return {'Status': status}


def list_all_power_supply_sensor_information():
    """Lists all power supply sensor information via the current session.

    Example:
    power_supply_sensors = list_all_power_supply_sensor_information()
    """
    status_code, headers, content = SessionCache.execute_get_request('/redfish/v1/Chassis/Self/Power')
    try:
        sensors = content['PowerSupplies']
    except KeyError:
        raise RuntimeError('Cannot list power supply sensors!')
    else:
        return sensors

def list_all_power_supply_sensor_status():
    """Lists all power supply sensor status via the current session.

    Example:
    power_supply_sensors_status = list_all_power_supply_sensor_status()
    """
    all_power_supply_info = list_all_power_supply_sensor_information()
    power_status = []
    for ps_info in all_power_supply_info:
        try:
            power_status.append({'Name': ps_info['Name'], 'Status': ps_info['Status']})
        except KeyError:
            raise RuntimeError('Cannot list power supply sensor status!')
    return power_status

def get_power_supply_sensor_information(sensor_name):
    """Gets the power supply sensor information with the given sensor name via the current session.

    Example:
    power_supply_sensor = get_power_supply_sensor_information("PSU0")
    """
    status_code, headers, content = SessionCache.execute_get_request('/redfish/v1/Chassis/Self/Power')
    try:
        sensor = next(item for item in filter(lambda p: p['Name'] == sensor_name, content['PowerSupplies']))
    except (KeyError, StopIteration):
        raise RuntimeError('Cannot get power supply sensor {}!'.format(sensor_name))
    else:
        return sensor


def list_all_voltage_sensor_information():
    """Lists all voltage sensor information via the current session.

    Example:
    voltage_sensors = list_all_voltage_sensor_information()
    """
    status_code, headers, content = SessionCache.execute_get_request('/redfish/v1/Chassis/Self/Power')
    try:
        sensors = content['Voltages']
    except KeyError:
        raise RuntimeError('Cannot list voltage sensors!')
    else:
        return sensors


def list_all_voltage_sensor_reading():
    """Lists all voltage sensor reading via the current session.

    Example:
    voltage_sensor_readings = list_all_voltage_sensor_reading()
    """
    try:
        readings = []
        for sensor in list_all_voltage_sensor_information():
            readings.append({'Name': sensor['Name'], 'ReadingVolts': sensor['ReadingVolts']})
    except (KeyError, RuntimeError):
        raise RuntimeError('Cannot list all voltage sensor readings!')
    else:
        return readings


def list_all_voltage_sensor_status():
    """Lists all voltage sensor status via the current session.

    Example:
    voltage_sensor_status = list_all_voltage_sensor_status()
    """
    try:
        status = []
        for sensor in list_all_voltage_sensor_information():
            status.append({'Name': sensor['Name'], 'Status': sensor['Status']})
    except (KeyError, RuntimeError):
        raise RuntimeError('Cannot list all voltage sensor status!')
    else:
        return status


def get_voltage_sensor_information(sensor_name):
    """Gets the voltage sensor information with the given sensor name of the current session.

    Example:
    voltage_sensor = get_voltage_sensor_information("Volt_P3V_BAT")
    """
    status_code, headers, content = SessionCache.execute_get_request('/redfish/v1/Chassis/Self/Power')
    try:
        sensor = next(item for item in filter(lambda v: v['Name'] == sensor_name, content['Voltages']))
    except (KeyError, StopIteration):
        raise RuntimeError('Cannot get voltage sensor!')
    else:
        return sensor


def get_voltage_sensor_reading(sensor_name):
    """Gets the voltage sensor reading with the given sensor name via the current session.

    Example:
    voltage_sensor_reading = get_voltage_sensor_reading("Volt_P3V_BAT")
    """
    try:
        reading = get_voltage_sensor_information(sensor_name)['ReadingVolts']
    except (KeyError, RuntimeError):
        raise RuntimeError('Cannot get voltage sensor reading!')
    else:
        return {'ReadingVolts': reading}


def get_voltage_sensor_status(sensor_name):
    """Gets the voltage sensor status with the given sensor name via the current session.

    Example:
    voltage_sensor_status = get_voltage_sensor_status("Volt_P3V_BAT")
    """
    try:
        status = get_voltage_sensor_information(sensor_name)['Status']
    except (KeyError, RuntimeError):
        raise RuntimeError('Cannot get voltage sensor status!')
    else:
        return {'Status': status}


def list_all_SEL_entries():
    """Lists all system event log entries via the current session.

    Example:
    entries = list_all_SEL_entries()
    """
    entries = []
    skip = 0
    collecting = True
    while collecting is True:
        all_sel_uri = '/redfish/v1/Managers/Self/LogServices/SEL/Entries?skip={}'.format(skip)
        status_code, headers, content = SessionCache.execute_get_request(all_sel_uri)
        try:
            entries.extend(content['Members'])
            if len(entries) < content['Members@odata.count']:
                skip += 50
            else:
                collecting = False
        except KeyError:
            raise RuntimeError('Cannot list SEL entries!')
    return entries


def get_SEL_entry(entry_id):
    """Gets the system event log with the given entry id via the current session.

    Example:
    entry = get_SEL_entry(42)
    """
    sel_entry_uri = '/redfish/v1/Managers/Self/LogServices/SEL/Entries/{}'.format(entry_id)
    status_code, headers, content = SessionCache.execute_get_request(sel_entry_uri)
    if status_code == httplib.NOT_FOUND:
        raise RuntimeError('Cannot get SEL entry!')
    else:
        return content


def clear_SEL_entries():
    """Clears the system event log entries via the current session.

    Example:
    clear_SEL_entries()
    """
    sel_post_uri = '/redfish/v1/Managers/Self/LogServices/SEL/Actions/LogService.ClearLog'
    sel_post_data = {"ClearType": "ClearAll"}
    status_code, headers, content = SessionCache.execute_post_request(sel_post_uri, data=sel_post_data)
    if status_code != httplib.NO_CONTENT:
        raise RuntimeError('Cannot clear SEL!')


def get_all_BIOS_attributes(future=False):
    """Gets all BIOS attributes via the current session (future values if future is True).

    Example:
    attributes = get_all_BIOS_attributes()
    """
    url = '/redfish/v1/Systems/Self/Bios'
    if future:
        url = '/redfish/v1/Systems/Self/Bios/SD'
    status_code, headers, content = SessionCache.execute_get_request(url)
    try:
        settings = content['Attributes']
    except KeyError:
        if not future:
            raise RuntimeError('Cannot get all BIOS settings!')
        else:
            return []
    else:
        return settings


def get_bios_attribute(attribute_name):
    """Gets the value of the given BIOS attribute via the current session.

    Example:
    attribute = get_bios_attribute("IPMI103")
    """
    status_code, headers, content = SessionCache.execute_get_request('/redfish/v1/Systems/Self/Bios')
    try:
        setting = content['Attributes'][str(attribute_name)]
    except KeyError:
        raise RuntimeError('Cannot get BIOS setting!')
    else:
        return setting


def set_BIOS_attributes(**attributes):
    """Sets the given BIOS attributes with the given values via the current session.

    Example:
    set_BIOS_attributes(IPMI103="Disabled", IPMI104="10 minutes")
    """
    bios_attributes_uri = '/redfish/v1/Systems/Self/Bios/SD'
    bios_data = {'Attributes': attributes}
    status_code, headers, content = SessionCache.execute_put_request(bios_attributes_uri, data=bios_data)
    if status_code != httplib.NO_CONTENT:
        raise RuntimeError('Cannot set BIOS attributes!')


def list_all_memory_information():
    """Lists all memory information via the current session.

    Example:
    memory_information = list_all_memory_information()
    """
    memory_information = []
    status_code, headers, content = SessionCache.execute_get_request('/redfish/v1/Systems/Self/Memory')
    try:
        for member in content['Members']:
            status_code, headers, content = SessionCache.execute_get_request(member['@odata.id'])
            memory_information.append(content)
    except KeyError:
        raise RuntimeError('Cannot list memory information!')
    else:
        return memory_information


def get_memory_count():
    """Returns the memory count via the current session.

    Example:
    memory_count = get_memory_count()
    """
    try:
        status_code, headers, content = SessionCache.execute_get_request('/redfish/v1/Systems/Self/Memory')
        count = content['Members@odata.count']
    except (KeyError, RuntimeError):
        raise RuntimeError('Cannot get memory count!')
    else:
        return count


def list_all_memory_size():
    """Lists all memory size via the current session.

    Example:
    memory_size = list_all_memory_size()
    """
    try:
        memory_size = []
        for memory in list_all_memory_information():
            memory_size.append({'Name': memory['Name'], 'CapacityMiB': memory['CapacityMiB']})
    except (KeyError, RuntimeError):
        raise RuntimeError('Cannot list all memory size!')
    else:
        return memory_size


def list_all_memory_status():
    """Lists all memory status via the current session.

    Example:
    memory_status = list_all_memory_status()
    """
    try:
        memory_status = []
        for memory in list_all_memory_information():
            memory_status.append({'Name': memory['Name'], 'Status': memory['Status']})
    except (KeyError, RuntimeError):
        raise RuntimeError('Cannot list all memory status!')
    else:
        return memory_status


def list_all_memory_type():
    """Lists all memory type via the current session.

    Example:
    memory_type = list_all_memory_type()
    """
    try:
        memory_type = []
        for memory in list_all_memory_information():
            memory_type.append({'Name': memory['Name'], 'MemoryDeviceType': memory['MemoryDeviceType']})
    except (KeyError, RuntimeError):
        raise RuntimeError('Cannot list all memory type!')
    else:
        return memory_type


def get_memory_information(memory_id):
    """Gets the memory information with the given memory id via the current session.

    Example:
    memory_information = get_memory_information(1)
    """
    memory_info_uri = '/redfish/v1/Systems/Self/Memory/{}'.format(memory_id)
    status_code, headers, content = SessionCache.execute_get_request(memory_info_uri)
    if status_code == httplib.NOT_FOUND:
        raise RuntimeError('Cannot get information for memory module {}!'.format(memory_id))
    else:
        return content


def get_memory_size(memory_id):
    """Gets the memory size with the given memory id via the current session.

    Example:
    memory_size = get_memory_size(1)
    """
    try:
        memory_size = get_memory_information(memory_id)['CapacityMiB']
    except (KeyError, RuntimeError):
        raise RuntimeError('Cannot get memory size!')
    else:
        return {'CapacityMiB': memory_size}


def get_memory_status(memory_id):
    """Gets the memory status with the given memory id via the current session.

    Example:
    memory_status = get_memory_status(1)
    """
    try:
        memory_status = get_memory_information(memory_id)['Status']
    except (KeyError, RuntimeError):
        raise RuntimeError('Cannot get memory status!')
    else:
        return {'Status': memory_status}


def get_memory_type(memory_id):
    """Gets the memory type with the given memory id via the current session.

    Example:
    memory_type = get_memory_type(1)
    """
    try:
        memory_type = get_memory_information(memory_id)['MemoryDeviceType']
    except (KeyError, RuntimeError):
        raise RuntimeError('Cannot get memory type!')
    else:
        return {'MemoryDeviceType': memory_type}


def list_all_BMC_network_interface_information():
    """Lists all BMC network interface information via the current session.

    Example:
    interface_information = list_all_BMC_network_interface_information()
    """
    interface_information = []
    status_code, headers, content = SessionCache.execute_get_request('/redfish/v1/Managers/Self/EthernetInterfaces')
    try:
        for member in content['Members']:
            status_code, headers, content = SessionCache.execute_get_request(member['@odata.id'])
            interface_information.append(content)
    except KeyError:
        raise RuntimeError('Cannot list BMC interface information!')
    else:
        return interface_information


def get_BMC_network_interface_information(interface):
    """Gets the BMC network interface information of the given interface via the current session.

    Example:
    interface_information = get_BMC_network_interface_information("usb0")
    """
    network_info_uri = '/redfish/v1/Managers/Self/EthernetInterfaces/{}'.format(interface)
    status_code, headers, content = SessionCache.execute_get_request(network_info_uri)
    if status_code == httplib.NOT_FOUND:
        raise RuntimeError('Cannot get BMC interface information!')
    else:
        return content


def list_all_blade_network_interface_information():
    """Lists all blade network interface information via the current session.

    Example:
    interface_information = list_all_blade_network_interface_information()
    """
    interface_information = []
    status_code, headers, content = SessionCache.execute_get_request('/redfish/v1/Systems/Self/EthernetInterfaces')
    try:
        for member in content['Members']:
            status_code, headers, content = SessionCache.execute_get_request(member['@odata.id'])
            interface_information.append(content)
    except KeyError:
        raise RuntimeError('Cannot list blade interface information!')
    else:
        return interface_information

def list_all_blade_network_interface_status():
    """Lists all blade network interface status via the current session.

    Example:
    interface_information = list_all_blade_network_interface_information()
    """
    nic_information = list_all_blade_network_interface_information()
    nic_status = []
    for nic_info in nic_information:
        try:
            nic_status.append({'Name': nic_info['Name'], 'Status': nic_info['Status']})
        except KeyError:
            raise RuntimeError('Cannot list blade status!')
    return nic_status

def get_blade_network_interface_information(interface):
    """Gets the blade network interface information of the given interface via the current session.

    Example:
    interface_information = get_blade_network_interface_information("eth0")
    """
    blade_query_uri = '/redfish/v1/Systems/Self/EthernetInterfaces/{}'.format(interface)
    status_code, headers, content = SessionCache.execute_get_request(blade_query_uri)
    if status_code == httplib.NOT_FOUND:
        raise RuntimeError('Cannot get blade interface information!')
    else:
        return content


def reset_BMC():
    """ POSTS reset command to BMC

    """
    reset_uri = '/redfish/v1/Managers/Self/Actions/Manager.Reset'
    reset_data = {'ResetType': 'IPMIColdReset'}
    status_code, headers, content = SessionCache.execute_post_request(reset_uri, data=reset_data)
    logging.info('reset_BMC: status_code=%s', status_code)


def power_cycle(reset_option='ForceRestart'):
    """ POSTS command to change the chassis power state
    """
    valid_reset_options = {'On', 'ForceOff', 'GracefulShutdown', 'ForceRestart'}
    if reset_option in valid_reset_options:
        reset_uri = '/redfish/v1/Chassis/Self/Actions/Chassis.Reset'
        reset_data = {'ResetType': reset_option}
        status_code, headers, content = SessionCache.execute_post_request(reset_uri, data=reset_data)
        logging.info('power_cycle: %s complete', reset_option)
    else:
        raise RuntimeError('power_cycle: invalid reset option ({})'.format(reset_option))

def chassis_power_on():
    """ Commands BMC to apply power to chassis.

    Example:
    chassis_power_on()
    """
    power_cycle('On')

def chassis_power_off():
    """ Commands BMC to remove power from chassis.

    Example:
    chassis_power_off()
    """
    power_cycle('ForceOff')

def chassis_restart():
    """ Commands BMC to restart the chassis

    Example:
    chassis_restart()
    """
    power_cycle('ForceRestart')

def chassis_shutdown():
    """ Commands BMC to shutdown the chassis

    Example:
    chassis_shutdown()
    """
    power_cycle('GracefulShutdown')

def get_chassis_power_state():
    """ Gets and extracts the status.state part of the Chassis/Self query
    Example:
    chassis_status = get_chassis_power_state()
    """
    query_uri = '/redfish/v1/Chassis/Self'
    status_code, headers, content = SessionCache.execute_get_request(query_uri)
    try:
        chassis_power_state = content['PowerState']
    except KeyError as key_err:
        raise RuntimeError(key_err)
    else:
        return {'State': chassis_power_state}


def get_boot_source_override_status():
    """ GETS boot source override status

    Example:
    boot_source = get_boot_source_override_status()
    """
    query_uri = '/redfish/v1/Systems/Self'
    status_code, headers, content = SessionCache.execute_get_request(query_uri)
    return content


def set_boot_source_override_target(boot_target, enable_option="Once"):
    """ Sets the boot source

    Example:
    set_boot_result = set_boot_source_override_target(boot_target, enable_option="Once")
    """
    set_boot_uri = '/redfish/v1/Systems/Self'
    boot_source_data = {
        'Boot': {
            'BootSourceOverrideEnabled': enable_option,
            'BootSourceOverrideTarget': boot_target
        }
    }
    boot_j = json.dumps(boot_source_data)
    status_code, headers, content = SessionCache.execute_patch_request(set_boot_uri, data=boot_j)
    logging.info('set_boot_source_override_target result: (%s)', content)


def list_all_system_information():
    """ Returns all high-level information from the /redfish/v1/Systems/Self part of the schema

    Example:
    all_system_info = list_all_system_information()
    """
    system_information_uri = '/redfish/v1/Systems/Self'
    status_code, headers, content = SessionCache.execute_get_request(system_information_uri)
    return content


def get_system_information():
    """ Returns filtered system information for the SDK

    Example:
    filtered_system_information = get_system_information()
    """
    all_system_information = list_all_system_information()
    try:
        system_information = {
            'Make': 'AirFrame',
            'Model': all_system_information['Model'],
            'Manufacturer': all_system_information['Manufacturer'],
            'ServiceTag': all_system_information['AssetTag']
        }
    except KeyError:
        raise RuntimeError('Cannot get system information!')
    else:
        return system_information


def get_BMC_ethernet_information():
    """ Gets all information for the BMC Ethernet interface

    Example:
    bmc_ethernet_info = get_BMC_ethernet_information()
    """
    get_uri = '/redfish/v1/Managers/Self/EthernetInterfaces/bond0'
    status_code, headers, content = SessionCache.execute_get_request(get_uri)
    return content


def get_BMC_MAC_address():
    """ Gets the BMC MAC address

    Example:
    bmc_mac_address = get_BMC_MAC_address()
    """
    bmc_ethernet_info = get_BMC_ethernet_information()
    try:
        mac_address = bmc_ethernet_info['MACAddress']
    except KeyError:
        raise RuntimeError('Cannot get BMC MAC address')
    else:
        return {'BMCMACAddress': mac_address}


def get_BMC_IPv4_address():
    """ Gets the IPv4 address of the BMC network interface

    Example:
    bmc_ipv4_address = get_BMC_IPv4_address()
    """
    bmc_ethernet_info = get_BMC_ethernet_information()
    try:
        ipv4_address_list = bmc_ethernet_info['IPv4Addresses']
    except KeyError:
        raise RuntimeError('Cannot get BMC IPv4 address list')
    else:
        return ipv4_address_list


def set_BMC_IPv4_address(ipv4_address, netmask, gateway):
    """ Sets IPv4 address, netmask and gateway IP addresses
    Returns the output of the patch request

    Example:
    set_result = set_BMC_IPv4_address('10.20.30.40', '255.255.0.0', '10.20.30.1')
    """
    patch_data = {
        'IPv4Addresses': {
            [
                {
                    'Address': ipv4_address,
                    'SubnetMask': netmask,
                    'Gateway': gateway,
                    'AddressOrigin': 'Static'
                }
            ]
        }
    }

    patch_uri = '/redfish/v1/Managers/Self/EthernetInterfaces/bond0'
    status_code, headers, content = SessionCache.execute_patch_request(patch_uri, patch_data)
    return content


def get_BMC_IPv6_address():
    """Gets the IPv6 address of the BMC network interface

    Example:
    bmc_ipv6_address = get_BMC_IPv6_address()
    """
    bmc_ethernet_info = get_BMC_ethernet_information()
    try:
        ipv6_address_list = bmc_ethernet_info['IPv6Addresses']
    except KeyError:
        raise RuntimeError('Cannot get BMC IPv6 address list')
    else:
        return ipv6_address_list


def set_BMC_IPv6_address(address, prefix_length=64):
    """Sets BMC IPv6 address

    Example:
    set_result = set_BMC_IPv6_address(address, 64)
    """
    raise RuntimeError('NotImplementedError: setBMC_IPv6_address')


def list_all_processor_information():
    """ Gets all information for all processors

    Example:
    all_processor_information = list_all_processor_information()
    """
    get_uri = '/redfish/v1/Systems/Self/Processors'
    status_code, headers, content = SessionCache.execute_get_request(get_uri)
    processor_information = []
    try:
        for member in content['Members']:
            member_uri = member['@odata.id']
            status_code, headers, m_content = SessionCache.execute_get_request(member_uri)
            processor_information.append(m_content)
    except KeyError:
        raise RuntimeError('Cannot list processor information!')
    return processor_information


def get_processor_information(processor_id):
    """ Returns all available information for processor indexed by processor_id

    Example:
    processor_1_info = get_processor_information(1)
    """

    processor_query_uri = '/redfish/v1/Systems/Self/Processors/{}'.format(processor_id)
    status_code, headers, content = SessionCache.execute_get_request(processor_query_uri)
    if status_code == httplib.NOT_FOUND:
        raise RuntimeError('Cannot get information for processor {}!'.format(processor_id))
    else:
        return content

def get_processor_count():
    """ Gets number of processors listed in Redfish schema

    Example:
    num_processors = get_processor_count()
    """
    all_processor_info = list_all_processor_information()
    processor_count = len(all_processor_info)
    return processor_count


def get_processor_status(processor_id):
    """ Gets the status record from the specified processor

    Example:
    processor_status = get_processor_status()
    """

    processor_info = get_processor_information(int(processor_id))
    try:
        processor_status = processor_info['Status']
    except KeyError:
        raise RuntimeError('Cannot determine status of processor {}'.format(int(processor_id)))
    else:
        return processor_status


def get_processor_name(processor_id):
    """ returns the name of the specified processor

    Example:
    processor_name = get_processor_name(2)
    """

    processor_info = get_processor_information(int(processor_id))
    try:
        processor_name = processor_info['Name']
    except (KeyError, IndexError):
        raise RuntimeError('Cannot determine name of processor {}'.format(int(processor_id)))
    else:
        return processor_name


def get_processor_brand(processor_id):
    """GETs the brand entry for specified processor

    Example:
    processor_brand = get_processor_brand(1)
    """
    processor_info = get_processor_information(int(processor_id))
    try:
        processor_brand = processor_info['Oem']['Intel_RackScale']['Brand']
    except KeyError:
        raise RuntimeError('Cannot determine brand of processor {}'.format(int(processor_id)))
    else:
        return processor_brand


def get_processor_version(processor_id):
    """GETs the version entry for specified processor

    Example:
    processor_version = get_processor_version(1)
    """
    processor_info = get_processor_information(int(processor_id))
    try:
        processor_version = processor_info['Oem']['Nokia_RackScale']['Version']
    except KeyError:
        raise RuntimeError('Cannot determine version of processor {}'.format(int(processor_id)))
    else:
        return processor_version


def get_processor_speed(processor_id):
    """Returns the speed of the specified processor

    Example:
    processor_speed = get_processor_speed(1)
    """
    processor_info = get_processor_information(int(processor_id))
    try:
        processor_speed = processor_info['MaxSpeedMHz']
    except KeyError:
        raise RuntimeError('Cannot determine speed of processor {}'.format(int(processor_id)))
    else:
        return processor_speed


def get_processor_cores(processor_id):
    """Returns the number of processor cores

    Example
    num_processor_cores = get_processor_cores(1)
    """
    processor_info = get_processor_information(int(processor_id))
    try:
        processor_core_count = processor_info['TotalCores']
    except KeyError:
        raise RuntimeError('Cannot determine number of cores in processor {}'.format(int(processor_id)))

    return processor_core_count


def list_all_processor_status():
    """ Returns status information for all processors

    Example:
    all_processor_status = list_all_processor_status()
    """
    all_processor_info = list_all_processor_information()
    processor_status = []
    try:
        for processor in all_processor_info:
            processor_status.append({'Name': processor['Name'], 'Status': processor['Status']})
    except (KeyError, IndexError, TypeError):
        raise RuntimeError('Cannot determine processor status')

    return processor_status


def get_chassis_information():
    """ Returns information from Chassis/Self part of schema

    Example:
    chassis_information = get_chassis_information()
    """
    chassis_information_uri = '/redfish/v1/Chassis/Self'
    status_code, headers, content = SessionCache.execute_get_request(chassis_information_uri)
    return content


def get_power_supply_count():
    """Returns the power supply count via the current session.

    Example:
    ps_count = get_power_supply_count()
    """
    try:
        count = len(list_all_power_supply_sensor_status())
    except RuntimeError:
        raise RuntimeError('Cannot get power supply count!')
    else:
        return count

def get_power_supply_sensor_status(psu_id):
    """Return the status of the power supply with the given id

    Example: ps_status = get_power_supply_sensor_status(1)
    """
    try:
        supplies = list_all_power_supply_sensor_status()
    except RuntimeError:
        raise RuntimeError('Cannot get power supply information!')
    try:
        supply = supplies[int(psu_id)]
    except RuntimeError:
        raise RuntimeError('Power supply index out of range!')
    try:
        if supply['Status']['State'] == 'Enabled':
            return supply['Status']['Health']
        else:
            return supply['Status']['State']
    except (KeyError, ValueError, TypeError):
        raise RuntimeError('Cannot get power supply status!')

def get_intrusion_status():
    """  Gets status of intrusion detection sensor
    example:
    intrusion_status = get_intrusion_status()
    """
    get_uri = '/redfish/v1/Chassis/Self'
    status_codes, headers, content = SessionCache.execute_get_request(get_uri)
    logging.debug('(get_intrusion_status) content = %s', content)
    try:
        return content['PhysicalSecurity']['IntrusionSensor']
    except KeyError as ky_err:
        raise RuntimeError('(get_intrusion_status) Cannot get intrusion status: %s', ky_err)


def list_all_disk_information():
    """ Lists all disk information

    Example:
    all_disk_information = list_all_disk_information()
    """
    all_disks_uri = '/redfish/v1/Chassis/Self/Drives'
    status_codes, headers, content = SessionCache.execute_get_request(all_disks_uri)
    disk_information = []
    drive_list = []
    try:
        for drive in content['Drives']:
            drive_list.append(drive['@odata.id'])
    except (KeyError, IndexError) as ki_err:
        raise RuntimeError('Cannot extract disk information: {}'.format(ki_err))
    try:
        for drive_uri in drive_list:
            status_codes, headers, m_content = SessionCache.execute_get_request(drive_uri)
            disk_information.append(m_content)
    except RuntimeError as rt_err:
        raise RuntimeError('Failure to get disk information: {}'.format(rt_err))

    return disk_information


def get_chassis_disk_count():
    """ Returns the number of disks reported in the Chassis/Self schema

    Example:
    num_disks = get_chassis_disk_count()
    """
    return len(list_all_disk_information())


def list_all_disk_status():
    """ Lists the status of all disks

    Example:
    disk_status = list_all_disk_status()
    """

    disk_information = list_all_disk_information()
    disk_status = []
    try:
        for disk_info in disk_information:
            disk_status.append({'Name': disk_info['Name'], 'Status': disk_info['Status']})
    except RuntimeError as rt_err:
        raise RuntimeError('Failure to get disk information: {}'.format(rt_err))
    else:
        return disk_status


def get_disk_information(disk_id):
    """ Returns information for specified disk drive

    Example:
    disk_info = get_disk_information(1)
    """
    disk_uri = '/redfish/v1/Chassis/Self/Drives/{}'.format(int(disk_id))
    status_codes, headers, content = SessionCache.execute_get_request(disk_uri)
    try:
        status_code, headers, content = SessionCache.execute_get_request(disk_uri)
    except RuntimeError as rt_err:
        raise RuntimeError('Failure to get disk information: {}'.format(rt_err))
    else:
        return content


def get_battery_status():
    """Gets the battery sensor status via the current session.

    Example:
    batteries = get_battery_status()
    """
    BATTERY_SENSORS = ['Volt_P3V_BAT']
    batteries = []
    sensors = list_all_voltage_sensor_information()
    for sensor in sensors:
        if sensor['Name'] in BATTERY_SENSORS:
            bat = {}
            bat['Name'] = sensor['Name']
            bat['Status'] = sensor['Status']
            bat['ReadingVolts'] = sensor['ReadingVolts']
            batteries.append(bat)
    return batteries


def get_registry_information():
    """Gets registry information via the current session.

    Example:
    registries = get_registry_information()
    """
    status_code, headers, content = SessionCache.execute_get_request('/redfish/v1/Registries')
    try:
        registries = content['Members']
    except KeyError:
        raise RuntimeError('Cannot get registry information!')
    else:
        return registries


def get_bios_settings():
    """Gets all bios settings and their descriptions + current/future values.

    Example:
    bios_settings = get_bios_settings()
    """
    bios_info = []
    reg_data = get_registry_information()
    for mem in reg_data:
        reg_url = mem['@odata.id']
        if 'BiosAttributeRegistry' in mem['@odata.id']:
            status_code, headers, content = SessionCache.execute_get_request(reg_url)
            try:
                for loc in content['Location']:
                    status_code, headers, content = SessionCache.execute_get_request(loc['Uri'])
                    bios_info = content['RegistryEntries']['Attributes']
                    break
            except KeyError:
                raise RuntimeError('Cannot get bios information!')

            break
    cur_values = get_all_BIOS_attributes()
    fut_values = get_all_BIOS_attributes(future=True)
    for bset in bios_info:
        if bset['AttributeName'] in cur_values:
            bset['CurrentValue'] = cur_values[bset['AttributeName']]
        if bset['AttributeName'] in fut_values:
            bset['FutureValue'] = fut_values[bset['AttributeName']]
    return bios_info


def list_user_accounts():
    """GETS information from all configured accounts

    Example
    all_user_account_information = list_user_accounts()
    """
    meta_uri = '/redfish/v1/AccountService/Accounts'
    status_code, headers, content = SessionCache.execute_get_request(meta_uri)
    account_uris = []
    try:
        for account_uri in content['Members']:
            account_uris.append(account_uri['@odata.id'])
    except KeyError:
        raise RuntimeError('list_user_accounts: Cannot get account information')
    else:
        account_information = []
        for account_uri in account_uris:
            status_code, headers, content = SessionCache.execute_get_request(account_uri)
            account_information.append(content)
        return account_information

def get_user_account_information(account_id):
    """ Gets user account information for the specified user

    Example
    user_2_information = get_user_account_information(2)
    """
    account_uri = 'redfish/v1/AccountService/Accounts/{}'.format(account_id)
    status_code, headers, content = SessionCache.execute_get_request(account_uri)
    return content

def get_unused_account_index():
    """ Checks list of used account numbers from API. Either returns
    an unused account index or raises an exception if no indicies are
    unused
    """
    try:
        account_info = list_user_accounts()
    except RuntimeError as rt_err:
        logging.error('get_unused_account_index) list_user_accounts(): %s', rt_err)
    else:
        occupied = set([int(acct['Id']) for acct in account_info])
        num_available_accounts = 16
        whole_set = set(range(1,num_available_accounts+1))
        avail_set = whole_set - occupied
        try:
            use_me = avail_set.pop()
        except KeyError as key_err:
            raise RuntimeError('No empty accounts. Please delete an account and retry')
        else:
            return use_me

def create_user_account(username, password, role="ReadOnly"):
    """Creates a new bmc user account

    Example:
    create_response = create_user_account('superuser', 'secret', 'Administrator')
    """
    valid_roles = {'Administrator', 'Operator', 'ReadOnly'}
    if role in valid_roles:
        account_data = {
            'UserName': username,
            'Password': password,
            'RoleId': role
        }
        logging.debug('(create_user_account) creating [%s %s %s]', username, password, role)
        create_uri = '/redfish/v1/AccountService/Accounts'

        status_code, headers, content = SessionCache.execute_post_request(create_uri, data=account_data)
        logging.debug('(create_user_account) status_code = %s', status_code)
        logging.debug('(create_user_account) header = %s', headers)
        return content
    else:
        raise RuntimeError('Invalid user role: {}'.format(role))


def set_user_account_password(account_username, new_password):
    """ Sets the password for the account specified by account_username

    Example:
    set_pw_result = set_user_account_password('admin', 'secret')
    """

    raise RuntimeError('(set_user_account_password) Not Implemented')
    # the following code is believed to be syntactically correct, but consistently fails due
    # to the redfish implementation's treatment of password as a read-only attribute.
    all_account_information = list_user_accounts()
    try:
        the_account = [acct for acct in all_account_information if acct['UserName'] == account_username]
        if len(the_account) == 0:
            account_id = None
        else:
            account_id = the_account[0]['Id']
    except KeyError:
        raise RuntimeError('(set_user_account_password) Cannot set password for {}'.format(account_username))
    else:
        if account_id is None:
            raise RuntimeError('(set_user_account_password) invalid username {}'.format(account_username))
        else:
            account_uri = '/redfish/v1/AccountService/Accounts/{}'.format(account_id)
            password_data = {'Password:': new_password, 'UserName':the_account[0]['UserName'], 'RoleId':'Operator', 'Enabled':True}

            status_code, headers, content = SessionCache.execute_patch_request(account_uri, data=password_data)

            logging.info('set password for %s', account_username)
            return content

def user_index_by_username(username):
    """
    Returns the UserName index for UserName==username, or raises
    a runtime exception if username isn't found
    """
    try:
        account_info = list_user_accounts()
    except RuntimeError as rt_err:
        logging.error('(user_index_by_username) list_user_accounts(): %s', rt_err)
    else:
        try:
            user_index = [acct['Id'] for acct in account_info if acct['UserName'] == username]
        except KeyError as key_err:
            raise RuntimeError('(user_index_by_username) keyword error: {}'.format(key_err))
        else:
            if len(user_index) == 0:
                raise RuntimeError('(user_index_by_username) {} not found'.format(username))
            else:
                return user_index[0]


def delete_user_account(account_username):
    """
    """
    delete_me = user_index_by_username(account_username)
    account_uri = '/redfish/v1/AccountService/Accounts/{}'.format(delete_me)
    status_code, headers, content = SessionCache.execute_delete_request(account_uri)
    logging.info('(delete_user_account) status_code = %s', status_code)
    return content

