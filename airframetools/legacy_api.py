"""
    Name: legacy_api.py
    Purpose: For OOB provisioning functions not supported in Redfish
    legacy_api provides non-Redfish BMC APIs
"""
#
#  Copyright 2018 - 2020 Nokia, Inc All rights reserved.
#  Library to support functionality not covered in the Redfish API
#  for Nokia AirFrame Servers
#
#
import json
import logging
import requests

from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


def open_session(host, username, password, timeout_sec=15):
    """
    Returns a dict containing a status boolean and the the object to be
    used in future accesses to the legacy bmc API.
    """
    #requests.packages.urllib3.disable_warnings()
    bmc_session = requests.Session()
    bmc_session.auth = (username, password)
    bmc_session.host = host
    credentials = {'username': username, 'password': password}
    login_uri = 'https://{}/api/session'.format(host)

    try:
        post_reply = bmc_session.post(login_uri, verify=False, timeout=timeout_sec, data=credentials)
    except requests.exceptions.RequestException as r_err:
        raise RuntimeError('(open_session) Error posting session request: {}'.format(r_err))
    else:
        logging.debug('(open_session) post_reply.status_code: %s', post_reply.status_code)
        logging.debug('(open_session) post_reply.headers: %s', post_reply.headers)
        logging.debug('(open_session) post_reply.content: %s', post_reply.content)

        prj = post_reply.json()
        if post_reply.ok:
            try:
                bmc_session.headers['X-CSRFTOKEN'] = prj['CSRFToken']
            except KeyError as key_err:
                logging.error('(open_session) CSRFToken missing from post response: {}'.format(key_err))
                return post_reply
        else:
            raise RuntimeError('(open_session) BMC signalled an error: {}'.format(prj['error']))

    return bmc_session

def close_session(session):
    """
    Closes the BMC session. Bookends open_session()
    """
    session_uri = 'https://{}/api/session'.format(session.host)
    try:
        delete_reply = session.delete(session_uri, headers=session.headers)
    except requests.exceptions.RequestException as r_err:
        raise RuntimeError('(close_session) error posting session delete: %s', r_err)
    else:
        dr_j = delete_reply.json()
        if delete_reply.ok:
            logging.debug('(close_session) DELETE reply: %s', dr_j)
        else:
            raise RuntimeError('(close_session) session DELETE failed: %s', dr_r['error'])
    session.close()

def get_firmware_version(bmc_session):
    """
    """
    bmc_query = 'http://{}/api/maintenance/firmware/verfication'.format(bmc_session.host)
    response = bmc_session.get(bmc_query)
    return response.json()


def get_fru(bmc_session):
    """
    Returns FRU information
    """
    bmc_query = 'https://{}/api/fru'.format(bmc_session.host)
    response = bmc_session.get(bmc_query)
    return response.json()

def legbmc_get_sensors(bmc_session):
    """
    Prints information about BMC Sensors
    """
    bmc_query = 'https://{}/api/sensors'.format(bmc_session.host)
    sensors = bmc_session.get(bmc_query)
    logging.info('legbmc_get_sensors: %s', sensors.text)

#--------------------------------
def get_users_info(bmc_session):
    """
    Returns the BMC's user list with all details. Return format is a python list
    containing a dictionary for each user
    """
    bmc_query = 'https://{}/api/settings/users'.format(bmc_session.host)
    response = bmc_session.get(bmc_query)
    return response.json()

def add_new_user(bmc_session, new_user_info):
    """
    adds a new user with admin rights. Returns the http PUT response
    """
    #sdk #180

    all_users = get_users_info(bmc_session)

    info_template = [uinfo for uinfo in all_users if uinfo['name'] == "admin"]
    free_slots = [uinfo['id'] for uinfo in all_users if uinfo['access'] == 0 and uinfo['name'] == '']

    if len(free_slots) < 1:
        raise RuntimeError('(add_new_user) no available slots for a new user')
    else:
        new_user = info_template[0]

    new_user['id'] = free_slots[0]
    new_user['name'] = new_user_info.uname
    new_user['password'] = new_user_info.passw
    new_user['confirm_password'] = new_user_info.passw
    new_user_j = json.dumps(new_user)
    headers = {'content-type': 'application/json'}
    bmc_cmd = 'https://{}/api/settings/users/{}'.format(bmc_session.host, str(free_slots[0]))
    response = bmc_session.put(bmc_cmd, data=new_user_j, headers=headers, verify=False)
    return response.json()

def user_by_username(bmc_session, username):
    """
    returns the BMC user record for username
    """
    all_users = get_users_info(bmc_session)
    the_user = [uslot for uslot in all_users if uslot['name'] == username]
    logging.debug('(user_by_username) the_user = %s', the_user)

    if len(the_user) == 0:
        raise RuntimeError('(user_by_username) user {} not found'.format(username))
    else:
        return the_user[0]

    
def change_user_password(bmc_session, username, new_password):
    """
    Changes password for username
    """
    logging.info('(change_user_password) changing password for %s to %s', username, new_password)
    # get the template for password request
    user_record = user_by_username(bmc_session, username)
    #add fields required for changing password
    user_record['UserOperation'] = 1
    user_record['changepassword'] = 1
    user_record['password'] = new_password
    user_record['confirm_password'] = new_password
    user_record['password_size'] = 'bytes_16'
    logging.debug('(change_user_password) user_record is %s', user_record)

    headers = {'content-type': 'application/json'}
    pass_cmd = 'https://{}/api/settings/users/{}'.format(bmc_session.host, user_record['id'])

    response = bmc_session.put(pass_cmd, data=json.dumps(user_record), headers=headers, verify=False)

    logging.debug('(change_user_password) ===========================================')
    logging.debug('(change_user_password) response.content: %s', response.content)
    logging.debug('(change_user_password) ===========================================')
    return response.json()

def set_user_properties(bmc_session, username, u_properties):
    """
    """
    logging.info('(set_user_properties) setting properties for %s', username)
    logging.debug('(set_user_properties) input is %s', u_properties)
    # get the template for password request
    user_record = user_by_username(bmc_session, username)
    # validate u_properties keys. This isn't a complete list of valid keys, but these have been tested
    valid_keys = set(['email_id', 'email_format'])
    up_keys = set(u_properties.keys())
    bad_keys = up_keys - valid_keys
    if len(bad_keys) > 0:
        raise RuntimeError('(set_user_properties) bad key error: {}'.format(bad_keys))

    #user_record.update(u_properties)
    user_record['UserOperation'] = 1

    u_properties['UserOperation'] = 1
    u_properties['ChangePassword'] = 1
 
    logging.debug('(set_user_properties) user_record is %s', user_record)
    headers = {'content-type': 'application/json'}

    prop_cmd = 'https://{}/api/settings/users/{}'.format(bmc_session.host, user_record['id'])

 #   response = bmc_session.put(prop_cmd, data=json.dumps(user_record), headers=headers, verify=False)
    response = bmc_session.put(prop_cmd, data=json.dumps(u_properties), headers=headers, verify=False)

    #response = bmc_session.put(pcmd, data=user_record, headers=headers, verify=False)

    logging.debug('(set_user_properties) ===========================================')
    logging.debug('(set_user_properties) response.content: %s', response.content)
    logging.debug('(set_user_properties) ===========================================')
 
    return response


def set_user_ssh_key(bmc_session, user_id):
    """
    Sets the bmc ssh key for the user specified by user_id
    Returns the HTTP response
    """
    bmc_query = 'https://{}/api/settings/user/ssh-key-upload/{}'.format(bmc_session.host, user_id)
    files = {'upload_ssh_key': open('id_rsa.pub', 'r')}
    response = bmc_session.post(bmc_query, files=files)
    return response.json()

def delete_user(bmc_session, username):
    """
        Legacy API to delete existing User
    """
    # First need to check if user exists
    # Find the users slot #
    # Delete that user account
    user_exist = False
    bmc_query = 'https://{}/api/settings/users'.format(bmc_session.host)
    try:
        response = bmc_session.get(bmc_query)
    except RuntimeError as rt_err:
        logging.error(rt_err)
    else:
        all_users = response.json()
    # Before creating a new user, should check that user doesn't exist.
    #  add here similar to lookinf for free slots
    # 
    user_exist = [uinfo['id'] for uinfo in all_users if uinfo['name'] == username]
    if user_exist:
        logging.info('User Exists')
        return user_exist
    # add Stub
    return response.json()

def get_media_general(bmc_session):
    """
    Returns the information on media settings
    Required for virtual media location and settings.
    """
    bmc_query = 'https://{}/api/settings/media/general'.format(bmc_session.host)
    response = bmc_session.get(bmc_query)
    data = response.json()
    return data

def set_media_general(bmc_session, media_data):
    """
    Provides a method for setting media options on the BMC including remote media location.
    """

    payload = json.dumps(media_data)
    logging.debug(payload)
    headers = {'content-type': 'application/json'}
    bmc_query = 'https://{}/api/settings/media/general'.format(bmc_session.host)
    response = bmc_session.put(bmc_query, data=payload, headers=headers, verify=False)
    return response.json()

# -------- PEF functions -------
def set_LAN_destination(bmc_session, LAN_specification):
    """
    """

    payload = { 
       "channel_id":'1',
       "id":'1',
       "destination_address":"",
       "name":"admin",
       "lan_channel":'1',
       "destination_type":"email",
       "subject":"",
       "message":"",
       "Format":"ami_format"
    }
    payload.update(LAN_specification)
    logging.debug('(set_LAN_destination) payload = %s', payload)

    uri = 'https://{}/api/settings/pef/lan_destinations/{}'.format(bmc_session.host, payload['id'])
    headers = {'content-type': 'application/json'}
    response = bmc_session.put(uri, data=json.dumps(payload), headers=headers, verify=False)

    return response.json()

def enable_event_filter(bmc_session, event_filter_specification):
    """
    """
    events = {
        "id":1,
        "enable_filter":1,
        "trigger_event_severity":"unspecified",
        "power_action":"none",
        "policy_group":2,
        "generator_id_1":255,
        "generator_id_2":255,
        "sensor_type":"Any",
        "name":"All Sensors",
        "sensor_num":255,
        "event_trigger":255,
        "event_data_1_offset_mask ":65535,
        "event_data_1_and_mask ":0,
        "event_data_1_compare_1":0,
        "event_data_1_compare_2":0,
        "event_data_2_and_mask ":0,
        "event_data_2_compare_1":0,
        "event_data_2_compare_2":0,
        "event_data_3_and_mask ":0,
        "event_data_3_compare_1":0,
        "event_data_3_compare_2":0,
        "raw_data":1,
        "generator_type":"slave",
        "event_options":"all",
        "slave_addr_software_id":"",
        "channel_number":"1",
        "ipmb_lun":"0"
    }
    events.update(event_filter_specification)
    logging.debug('(enable_event_filter) event payload: %s', events)

    uri = 'https://{}/api/settings/pef/event_filters/{}'.format(bmc_session.host, events['id'])
    headers = {'content-type': 'application/json'}
    response = bmc_session.put(uri, data=json.dumps(events), headers=headers, verify=False)
    logging.debug('(enable_event_filter) response: %s', response.content)
    return response.json()


# ----- SMTP functions --------
def get_smtp_attributes(bmc_session):
    """
    Returns the BMC SMTP configuration
    """
    bmc_query = 'https://{}/api/settings/smtp'.format(bmc_session.host)
    response = bmc_session.get(bmc_query)
    return response.json()

def set_smtp_attributes(bmc_session, bmc_smtp_cfg):
    """
    Sets the BMC SMTP configuration based on the bmc_smtp_cfg parameter.
    Assumes the BMC supports only one configuration set consisting of one
    primary and one secondary configuration.
    """
    smtp_data = json.dumps(bmc_smtp_cfg)
    headers = {'content-type': 'application/json'}
    bmc_cmd = 'https://{}/api/settings/smtp/{}'.format(bmc_session.host, bmc_smtp_cfg['id'])
    response = bmc_session.put(bmc_cmd, data=smtp_data, headers=headers, verify=False)
    return response.json()

# ------------ Network Address functions ----------------------
def get_network_settings(bmc_session):
    """
    Returns network settings for the BMC
    """
    bmc_query = 'https://{}/api/settings/network'.format(bmc_session.host)
    response = bmc_session.get(bmc_query)
    return response

def default_ipv6_config(network_settings):
    """
    Load a dictionary with default IPv6 configuration. The caller can use the
    returned dictionary as a template before the call to set_bmc_ipv6().
    Call get_network_settings to get the 'network_settings' argument
    """
    try:
        if network_settings['interface_name'] == 'bond0':
            default_configuration = {
                "id": network_settings['id'],
                "interface_name": network_settings['interface_name'],
                "channel_number": network_settings['channel_number'],
                "lan_enable": network_settings['lan_enable'],
                "mac_address": network_settings['mac_address'],
                "ipv4_enable": network_settings['ipv4_enable'],
                "ipv4_dhcp_enable": network_settings['ipv4_dhcp_enable'],
                "ipv4_address": network_settings['ipv4_address'],
                "ipv4_subnet": network_settings['ipv4_subnet'],
                "ipv4_gateway": network_settings['ipv4_gateway'],
                "ipv6_enable": network_settings['ipv6_enable'],
                "ipv6_dhcp_enable": network_settings['ipv6_dhcp_enable'],
                "ipv6_address": network_settings['ipv6_address'],
                "ipv6_index": network_settings['ipv6_index'],
                "ipv6_prefix": network_settings['ipv6_prefix'],
                "ipv6_gateway": network_settings['ipv6_gateway'],
                "vlan_enable": network_settings['vlan_enable'],
                "vlan_id": network_settings['vlan_id'],
                "vlan_priority": network_settings['vlan_priority']
            }
            return default_configuration
        else:
            if_name = network_settings.interface_name
            raise RuntimeError('Network settings from wrong interface ({}, should be bond0)'.format(if_name))
    except KeyError:
        raise RuntimeError('Missing information from network_settings')


def set_bmc_ipv6(bmc_session, ipv6_configuration):
    """
        Create IPv6 Address using Legacy API
    """
    bmc_request = 'https://{}/api/settings/network/{}'.format(bmc_session.host, ipv6_configuration['id'])
    headers = {'content-type': 'application/json'}
    ipv6_config_j = json.dumps(ipv6_configuration)
    response = bmc_session.put(bmc_request, data=ipv6_config_j, headers=headers, verify=False)
    return response
