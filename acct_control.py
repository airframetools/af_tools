"""
name: acct_control
This script interacts with user accounts.
It is useful for testing BMC credential management.
"""
#
# Copyright 2018-2020 Nokia, Inc. All rights reserved.
#

from __future__ import print_function
import argparse
import logging
import time
import json
import airframetools.redfish_api as redfish
import airframetools.legacy_api as legapi

SKIP_ME = -2
REDFISH_API = 'redfish'
LEGACY_API = 'legacy'
 
def do_delete(cmd_line):
    """
    """
    delete_start = time.time()
    logging.info('(do_delete) deleting account %s', cmd_line.delete_cmd)
    try:
        delete_result = redfish.delete_user_account(cmd_line.delete_cmd)
    except RuntimeError as rt_err:
        logging.error('(do_delete) %s', rt_err)
    else:
        logging.info('(do_delete) delete_result = %s', delete_result)

    delete_end = time.time()
    delete_runtime = delete_end - delete_start
    logging.info('(do_delete) runtime = %s', delete_runtime)

def do_create_user_account(cmd_line):
    """
    """
    create_start = time.time()
    logging.info('(do_create) cmd_line [%s %s %s]', cmd_line.create_cmd[0], cmd_line.create_cmd[1], cmd_line.create_cmd[2])
    try:
        create_result = redfish.create_user_account(cmd_line.create_cmd[0], cmd_line.create_cmd[1], cmd_line.create_cmd[2])
    except RuntimeError as rt_err:
        logging.error('(do_create) failure creating user account: %s', rt_err)
    else:
        #logging.info('(do_create) result: %s', create_result)
        print(create_result)

    create_end = time.time()
    create_runtime = create_end-create_start
    logging.info('(do_create) request elapsed time = %s', create_runtime)


def do_find_username(cmd_line):
    """
    """
    uname_start = time.time()
    logging.info('(do_find_username) looking for %s', cmd_line.uname_cmd)
    try:
        p = redfish.user_index_by_username(cmd_line.uname_cmd)
    except RuntimeError as rt_err:
        logging.error('(do_find_username) %s', rt_err)
    else:
        print(p)

    uname_end = time.time()
    uname_runtime = uname_end - uname_start
    logging.info('(do_find_username) request elapsed time = %s', uname_runtime)

def do_change_password(cmd_line, which_api, legacy_session=None):
    """
    """
    uname = cmd_line.passw_cmd[0]
    passw = cmd_line.passw_cmd[1]
    logging.info('(do_change_password) changing password for %s to %s', uname, passw)
    try:
        if which_api == REDFISH_API:
            logging.debug('(do_change_password) using redfish API')
            result = redfish.set_user_account_password(uname, passw)
        else:
            logging.debug('(do_change_password) using legacy API')
            result = legapi.change_user_password(legacy_session, uname, passw)
    except RuntimeError as rt_err:
        logging.error('(do_change_password) error: %s', rt_err)
    else:
        print(result)

def do_add_email(args, legacy_session):
    """
    """
    start_t = time.time()
    username = args.add_email[0]
    new_email_address = args.add_email[1].decode('utf-8')
    email_d = {'email_id': new_email_address}
    response = legapi.set_user_properties(legacy_session, username, email_d)
    end_t = time.time()
    x_time = end_t - start_t
    logging.info('(do_add_email) execution time = %s', x_time)
    print(response.content)


def main():
    """
    """
    starttime = time.time()

    parser = argparse.ArgumentParser(description='Nokia (c)2018 AirFrame(R) information tool')
    parser.add_argument('ipadd', help='ip address')
    parser.add_argument('uname', help='username')
    parser.add_argument('passw', help='password')
    parser.add_argument('-a', '--api_choice', nargs='?', default=SKIP_ME, help='choose api (redfish or legacy)')
    parser.add_argument('-c', '--create_cmd', nargs='+', default=SKIP_ME, help='create <name> <password> <role>')
    parser.add_argument('-d', '--delete_cmd', nargs='?', default=SKIP_ME, help='delete <user_id>')
    parser.add_argument('-e', '--add_email', nargs='+', default=SKIP_ME, help='add email address for <username>')
    parser.add_argument('-i', '--uname_cmd', nargs='?', default=SKIP_ME, help='find index for <username>')
    parser.add_argument('-p', '--passw_cmd', nargs='+', default=SKIP_ME, help='change password for username')
    parser.add_argument('-z', '--enable_dbg', nargs='?', default=SKIP_ME, help='enable debug logging mode')

    args = parser.parse_args()

    if args.enable_dbg != SKIP_ME:
        logging.basicConfig(level=logging.DEBUG)
        logging.debug('debug-mode logging enabled')
    else:
        logging.basicConfig(level=logging.INFO)

    ok_apis = [REDFISH_API, LEGACY_API]
    if args.api_choice != SKIP_ME:
        if args.api_choice in ok_apis:
            use_api = args.api_choice
        else:
            logging.error('(main) invalid API choice: %s', args.api_choice)
            exit()
    else:
        use_api = None
    if use_api != None:
        logging.debug('(main) using %s API exclusively', use_api)
    else:
        logging.debug('(main) neither API chosen')


    if use_api != LEGACY_API:
        logging.debug('====== in redfish segment, use_api = %s', use_api)
        try:
            redfish.open_session(args.ipadd, username=args.uname, password=args.passw)
        except RuntimeError as rt_err:
            logging.error('(main) failure opening redfish session (%s)', rt_err)
        else:
            if args.create_cmd != SKIP_ME:
                do_create_user_account(args)

            if args.delete_cmd != SKIP_ME:
                do_delete(args)

            if args.uname_cmd != SKIP_ME:
                do_find_username(args)

            if args.passw_cmd != SKIP_ME and use_api == REDFISH_API:
                do_change_password(args, use_api)

            redfish.terminate_current_session()

    try:
        legacy_session = legapi.open_session(host=args.ipadd, username=args.uname, password=args.passw)
    except RuntimeError as rt_err:
        logging.error('(main) failure opening legacy session: %s', rt_err)
    else:
        if args.passw_cmd != SKIP_ME and use_api != REDFISH_API:
            print(" ++++++++++++++++++++++++ WTF ++++++++++++++++++++++")
            print('   api passed to change password is {}'.format(LEGACY_API))
            do_change_password(args, LEGACY_API, legacy_session)
        if args.add_email != SKIP_ME:
            do_add_email(args, legacy_session)

        legapi.close_session(legacy_session)

    endtime = time.time()
    runtime = endtime-starttime
    logging.info('(main) execution time: %s', runtime)

if __name__ == '__main__':
    main()
