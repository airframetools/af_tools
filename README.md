Copyright 2018-2020 Nokia, Inc. All right reserved.

# AirFrame Tools
The Airframe tools and python scripts are 

# How to Use tool

***afp_wrench*** is a commandline tool that implements the clie side of the Redfish RESTful API for Data Center Hardware Management.
There are a set of libraries that form the Airframe-tools SDK used to call Redfish API's or BMC API's to be integrated in larger scale provisioing projects.  The ***afp_wrench*** tool also facilitates use in scripts.


**Redfish** is the new RESTful API for hardware management defined by the DMTF Scalable Platform Management Forum (SPMF).  It provides a modern, secure, multi-node, extendable interface for doing hardware management.  The initial release included hardware inventory, server power-on/off/reset, reading power draw, setting power limits, reading sensors such as fans, read/write of ID LEDs, asset tags, and went beyond IPMI in functionality to include inventory of processors, storage, Ethernet controllers, and total memory.
(not all these features are supported in the initial release of Airframe Tools SDK)
 

## Usage:
 ***python***  ***afp_wrench***  <ipadd> <uname> <passw> <argument>

 * **ipadd** IP address of Server
 * **uname** Username
 * **passw** Password
 * **argument**  Other Options

### Common OPTIONS:
    
    -s, --status
    -p, --power  <p>  power supply information
    -c, --cpu <c>     cpu information
    -m, --memory <m>  memory information  
    -d, --disk <d>    disk information
    -f, --fans <f>    fan information
    -t, --temp <t>    temperature sensor information  
    -v, --verbose     verbose mode
    -i, --bios        BIOS Settings 

The parameters enclosed in the brackets (<>) are optional device index arguments. If the device index arguments are omitted, these reports return information for all devices within the class.

The data for all reports is presented in JSON-encoded format.
