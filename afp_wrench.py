"""
Name: afp_wrench.py
afp_wrench is the Python tool based on the AirFrame tools SDK for AirFrame
out of band maintenance.
"""
#
# Copyright 2018 - 2020 Nokia, Inc All right reserved.
# afp_wrench is a command line tool that enables a user to access the AirFrame tools Redfish/BMC SDK
# to manage AirFrame servers
#

from __future__ import print_function
import json
import argparse
import logging
import airframetools.redfish_api as redfish

SKIP_ME = -2

# -----------------------------
def do_accounts(cmd_line):
    """
        returns inforamtion about BMC operator accounts
    """
    try:
        redfish.open_session(cmd_line.ipadd, username=cmd_line.uname, password=cmd_line.passw)
    except RuntimeError as rt_err:
        logging.error('do_accounts: failure opening session: %s', rt_err)
    else:
        try:
            account_info = redfish.list_user_accounts()
        except RuntimeError as rt_err:
            logging.error('do_accounts: failure getting account information (%s)', rt_err)
        else:
            account_out = json.dumps(account_info)
            print(account_out)


# -----------------------------
def do_system_status(cmd_line):
    """
    Query system and chassis, then processor, memory, storage, power, thermal
    and drives attributes, filter, then present output as json
    """
    # tools220-280

    try:
        redfish.open_session(cmd_line.ipadd, username=cmd_line.uname, password=cmd_line.passw)
    except RuntimeError as rt_err:
        logging.error('do_system_status: failure opening session: %s', rt_err)
    else:
        try:
            health_sum = {}
            health_sum['Processors'] = redfish.list_all_processor_status()
            health_sum['Memory'] = redfish.list_all_memory_status()
            health_sum['Power'] = redfish.list_all_power_supply_sensor_status()
            health_sum['Fans'] = redfish.list_all_fan_sensor_status()
            health_sum['Temp'] = redfish.list_all_temperature_sensor_status()
            health_sum['Drives'] = redfish.list_all_disk_status()
            health_sum['Blade_NICs'] = redfish.list_all_blade_network_interface_status()
            hsj = json.dumps(health_sum)
            print(hsj)
        except RuntimeError as rte:
            logging.error('do_system_status: failure retrieving status (%s)', rte)
        redfish.terminate_current_session()

# -----------------------------

def do_power_status(cmd_line):
    """
    Calls Redfish API for power supply status, filters response for wattage
    and capacity, presents output as a json-encoded string.
    TODO: requirements call for amperage.
    """
    # tool requirement #210.

    try:
        redfish.open_session(cmd_line.ipadd, username=cmd_line.uname, password=cmd_line.passw)
    except RuntimeError as rt_err:
        logging.error('do_power_status: failure opening session (%s)', rt_err)
    else:
        try:
            if cmd_line.power is None:
                power_info = redfish.list_all_power_supply_sensor_information()
            else:
                power_info = redfish.get_power_supply_sensor_information(cmd_line.power)
            print(json.dumps(power_info))
        except RuntimeError as rt_err:
            logging.error('do_power_status: failure getting information (%s)', rt_err)
        redfish.terminate_current_session()


def do_cpu_status(cmd_line):
    """
    Get CPU information
    """

    try:
        redfish.open_session(cmd_line.ipadd, username=cmd_line.uname, password=cmd_line.passw)
    except RuntimeError as rt_err:
        logging.error('Session Open Error: %s', rt_err)
    else:
        if cmd_line.cpu is None:
            try:
                cpu_information = redfish.list_all_processor_information()
            except RuntimeError as rt_err:
                logging.error('do_cpu_status: failure getting cpu information: %s', rt_err)
            else:
                print(json.dumps(cpu_information))
        else:
            try:
                cpu_information = redfish.get_processor_information(int(cmd_line.cpu))
            except RuntimeError as rt_err:
                logging.error('do_cpu_status: failure getting cpu %s information: %s', cmd_line.cpu, rt_err)
            else:
                print(json.dumps(cpu_information))
        redfish.terminate_current_session()


def do_memory_status(cmd_line):
    """
    Get Memory Status
    """

    try:
        redfish.open_session(cmd_line.ipadd, username=cmd_line.uname, password=cmd_line.passw)
    except RuntimeError as rt_err:
        logging.error('do_memory_status: failure opening session: %s', rt_err)
    else:
        if cmd_line.memory is None:
            try:
                memory_information = redfish.list_all_memory_information()
            except RuntimeError as rt_err:
                logging.error('do_memory_status: failure getting information: %s', rt_err)
            else:
                print(json.dumps(memory_information))

        else:
            try:
                memory_information = redfish.get_memory_information(int(cmd_line.memory))
            except RuntimeError as rt_err:
                logging.error('do_memory_status: failure getting status: %s', rt_err)
            else:
                print(json.dumps(memory_information))
        redfish.terminate_current_session()

def do_nic_status(cmd_line):
    """
    Get NIC status
    """

    try:
        redfish.open_session(cmd_line.ipadd, username=cmd_line.uname, password=cmd_line.passw)
    except RuntimeError as rte:
        logging.error('do_nic_status: failure opening sessions: (%s)', rte)
    else:
        try:
            if cmd_line.nic is None:
                nic_info = redfish.list_all_blade_network_interface_information()
            else:
                nic_info = redfish.get_blade_network_interface_information(cmd_line.nic)
            print(json.dumps(nic_info))
        except RuntimeError as rt_err:
            logging.error('do_nic_status: failure getting information (%s)', rt_err)
        redfish.terminate_current_session()

def do_disk_status(cmd_line):
    """
    Get Disk Status
    """
    try:
        redfish.open_session(cmd_line.ipadd, username=cmd_line.uname, password=cmd_line.passw)
    except RuntimeError as rt_err:
        logging.error('do_disk_status: failure opening session: (%s)', rt_err)
    else:
        try:
            if cmd_line.disk is None:
                disk_status = redfish.list_all_disk_information()
            else:
                disk_status = redfish.get_disk_information(int(cmd_line.disk))
            print(json.dumps(disk_status))
        except RuntimeError as rt_err:
            logging.error('do_disk_status: failure getting information (%s)', rt_err)
        redfish.terminate_current_session()


def do_fan_status(cmd_line):
    """
    Get Fan Status
    """
    try:
        redfish.open_session(cmd_line.ipadd, username=cmd_line.uname, password=cmd_line.passw)
    except RuntimeError as rt_err:
        logging.error('do_fan_status: failure opening session (%s)', rt_err)
    else:
        try:
            if cmd_line.fan is None:
                fan_status = redfish.list_all_fan_sensor_information()
            else:
                fan_status = redfish.get_fan_sensor_information(cmd_line.fan)
            print(json.dumps(fan_status))
        except RuntimeError as rt_err:
            logging.error('do_fan_status: failure getting information (%s)', rt_err)

        redfish.terminate_current_session()


def do_temp_status(cmd_line):
    """
    Get Temperature Status
    """
    try:
        redfish.open_session(cmd_line.ipadd, username=cmd_line.uname, password=cmd_line.passw)
    except RuntimeError as rt_err:
        logging.error('do_temp_status: failure opening session (%s)', rt_err)
    else:
        try:
            if cmd_line.temp is None:
                temp_status = redfish.list_all_temperature_sensor_information()
            else:
                temp_status = redfish.get_temperature_sensor_information(cmd_line.temp)
            print(json.dumps(temp_status))
        except RuntimeError as rt_err:
            logging.error('do_temp_status: failure getting information (%s)', rt_err)
        redfish.terminate_current_session()


def do_battery_status(cmd_line):
    """
    Get Battery Status
    """
    try:
        redfish.open_session(cmd_line.ipadd, username=cmd_line.uname, password=cmd_line.passw)
    except RuntimeError as rt_err:
        logging.error('do_battery_status: failure opening session (%s)', rt_err)
    else:
        try:
            bat_status = redfish.get_battery_status()
            print(bat_status)
        except RuntimeError as rt_err:
            logging.error('Failure getting battery status (%s)', rt_err)
        redfish.terminate_current_session()

def do_bios_settings(cmd_line):
    """
    Get BIOS settings
    """
    try:
        redfish.open_session(cmd_line.ipadd, username=cmd_line.uname, password=cmd_line.passw)
    except RuntimeError as rt_err:
        logging.error('do_bios_settings: failure opening session (%s)', rt_err)
    else:
        try:
            bios_settings = redfish.get_bios_settings()
            print(json.dumps(bios_settings))
        except RuntimeError as rt_err:
            logging.error('do_bios_settings: failure getting battery status (%s)', rt_err)
        redfish.terminate_current_session()


def do_reboot(cmd_line):
    """
    Send command to reboot the bmc server
    """

    try:
        redfish.open_session(cmd_line.ipadd, username=cmd_line.uname, password=cmd_line.passw)
        redfish.reset_BMC()
    except RuntimeError as rt_err:
        logging.error('do_reboot, failure opening session: %s', rt_err)
    else:
        redfish.terminate_current_session()
        logging.info('do_reboot: session terminated')


def do_power_state(cmd_line):
    """
    """
    valid_commands = ['query', 'on', 'off', 'restart', 'shutdown']
    if cmd_line.pcmd in valid_commands:
        try:
            redfish.open_session(cmd_line.ipadd, username=cmd_line.uname, password=cmd_line.passw)
        except RuntimeError as rt_err:
            logging.error('do_reboot, failure opening session: %s', rt_err)
        else:
            if cmd_line.pcmd == 'query':
                try:
                    chassis_state = redfish.get_chassis_power_state()
                except RuntimeError as rt_err:
                    logging.error('Cannot get chassis_state: %s', rt_err)
                else:
                    print(chassis_state)
            if cmd_line.pcmd == 'on':
                try:
                    redfish.chassis_power_on()
                except RuntimeError as rt_err:
                    logging.error('Cannot apply power to chassis: %s', rt_err)
                else:
                    logging.info('Chassis powered on')
            if cmd_line.pcmd == 'off':
                try:
                    redfish.chassis_power_off()
                except RuntimeError as rt_err:
                    logging.error('Cannot remove power from chassis: %s', rt_err)
                else:
                    logging.info('Chassis powered off')
            if cmd_line.pcmd == 'restart':
                try:
                    redfish.chassis_restart()
                except RuntimeError as rt_err:
                    logging.error('Cannot restart chassis: %s', rt_err)
                else:
                    logging.info('Chassis restarted')
            if cmd_line.pcmd == 'shutdown':
                try:
                    redfish.chassis_shutdown()
                except RuntimeError as rt_err:
                    logging.error('Cannot shutdown chassis: %s', rt_err)
                else:
                    logging.info('Chassis shutdown')

            redfish.terminate_current_session()
            logging.info('do_power_state: session terminated')
    else:
        logging.error('do_power_state: invalid command (%s)', cmd_line.pcmd)
        logging.error('  valid commands are %s', valid_commands)

# -----------------------------
def do_bmc_version(cmd_line):
    """
    Query bmc version
    """

    try:
        redfish.open_session(cmd_line.ipadd, username=cmd_line.uname, password=cmd_line.passw)
    except RuntimeError as rt_err:
        logging.error('do_system_status: failure opening session: %s', rt_err)
    else:
        try:
            bmc_firmware_version = redfish.get_bmc_version()
            bios_version = redfish.get_bios_version()
            bmc_version = bmc_firmware_version.copy()
            bmc_version.update(bios_version)
        except RuntimeError as rte:
            logging.error('do_system_status: failure retrieving status (%s)', rte)
        else:
            bmcvj = json.dumps(bmc_version)
            print(bmcvj)
        redfish.terminate_current_session()

def do_load_bmc_firmware(firmware_uri):
    """
    """
    try:
        logging.info('(do_load_bmc_firmware) preserving configuration')
        redfish.set_bmc_configuration_preservation(Authentication=True, IPMI=True, KVM=True, \
                        Network=True, SEL=True, SNMP=True, SSH=True)
        logging.info('(do_load_bmc_firmware) loading firmware')
        redfish.update_bmc_firmware(firmware_uri)
    except RuntimeError as rt_err:
        logging.error('(do_load_bmc_firmware) error: %s', rt_err)
 
def do_load_bios_firmware(bios_image_uri):
    """
    """
    try:
        logging.info('(do_load_bios_firmware) powering host down')
        #ps_cmd.pcmd = 'off'
        #do_power_state(ps_cmd)
        redfish.chassis_power_off()
        logging.info('(do_load_bios_firmware) downloading BIOS image')
        redfish.update_bios_firmware(bios_image_uri)
    except RuntimeError as rt_err:
        logging.info('(do_load_bios_firmware) error: %s', rt_err)


VALID_FWARE_TARGETS = ['bios', 'bmc']
def do_load_firmware(cmd_line):
    """"
    """
    if(cmd_line.load_firmware[0] in VALID_FWARE_TARGETS):
        try:
            redfish.open_session(cmd_line.ipadd, username=cmd_line.uname, password=cmd_line.passw)
        except RuntimeError as rt_err:
            logging.error('(do_load_firmware) error opening redfish session: %s', rt_err)
        else:
            logging.info('(do_load_firmware) loading %s with file stored at %s', \
                    cmd_line.load_firmware[0], cmd_line.load_firmware[1])
            if cmd_line.load_firmware[0]=='bmc':
                do_load_bmc_firmware(cmd_line.load_firmware[1])
            else:
                do_load_bios_firmware(cmd_line.load_firmware[1])
    else:
        logging.error('(do_load_firmware) invalid firmware target: %s', cmd_line.load_firmware[0])

def main():
    """
        ***python***  ***afp_wrench***  <ipadd> <uname> <passw> <argument>

        * **ipadd** IP address of Server
        * **uname** Username
        * **passw** Password
        * **argument**  Other Options

        ### Common OPTIONS:

          -a, --accounts                  Account information
          -b, --battery                   Battery status
          -c [CPU], --cpu [CPU]           CPU report. [CPU] is optional CPU index
          -d [DISK], --disk [DISK]        Disk report. [DISK] is optional drive index.
          -f [FAN], --fan [FAN]           Fan report. [FAN] is optional fan index.
          -i, --bios                      BIOS settings
          -m [MEMORY], --memory [MEMORY]  Memory report. [MEMORY] is optional module index.
          -n [NIC], --nic [NIC]           Network interfaces. [NIC] is the interface name (e.g., 'eth0')
          -p [POWER], --power [POWER]     Power supply report. [POWER] is optional power supply name.
          -r, --reboot                    Reboot BMC
          -s, --status                    System health and status
          -t [TEMP], --temp [TEMP]        Temperature reports. [TEMP] is optional sensor name.


        Example:  afp_wrench.py -i 127.0.0.0 admin secret | python -m json.tool
    """
    #docs = Documentation()
    #help(docs.__module__)

    logging.basicConfig(level=logging.INFO)

    parser = argparse.ArgumentParser(description='Nokia (c)2018 AirFrame(R) information tool')
    parser.add_argument('ipadd', help='ip address')
    parser.add_argument('uname', help='username')
    parser.add_argument('passw', help='password')
    parser.add_argument('-a', '--accounts', help='account information', action='store_true')
    parser.add_argument("-b", "--battery", help="battery status", action='store_true')
    parser.add_argument("-c", "--cpu", nargs='?', default=SKIP_ME, \
          help="CPU report. [CPU] is optional CPU index")
    parser.add_argument("-d", "--disk", nargs='?', default=SKIP_ME, \
          help="Disk report. [DISK] is optional drive index.")
    parser.add_argument("-f", "--fan", nargs='?', default=SKIP_ME, \
          help="Fan report. [FAN] is optional fan index.")
    parser.add_argument("-i", "--bios", help="BIOS settings", action='store_true')
    parser.add_argument("-l", "--load_firmware", nargs="+", default=SKIP_ME, help="Load Firmware")
    parser.add_argument("-m", "--memory", nargs='?', default=SKIP_ME, \
          help="Memory report. [MEMORY] is optional module index.")
    parser.add_argument("-n", "--nic", nargs="?", default=SKIP_ME, help="Network interfaces")
    parser.add_argument("-p", "--power", nargs='?', default=SKIP_ME, \
          help="power supply report. [POWER] is optional power supply name (e.g., 'PSU0').")
    parser.add_argument("-q", "--pcmd", nargs="?", default=SKIP_ME, \
          help="query/change chassis power state")
    parser.add_argument("-r", "--reboot", help="Reboot BMC", action='store_true')
    parser.add_argument("-s", "--status", help="system health and status", action='store_true')
    parser.add_argument("-t", "--temp", nargs='?', default=SKIP_ME, \
          help="Temperature reports. [TEMP] is optional sensor name.")
    parser.add_argument("-v", "--bmc_version", help="BMC version", action ='store_true')

    args = parser.parse_args()

    if args.power != SKIP_ME:
        do_power_status(args)

    if args.status:
        do_system_status(args)

    if args.cpu != SKIP_ME:
        do_cpu_status(args)

    if args.memory != SKIP_ME:
        do_memory_status(args)

    if args.nic != SKIP_ME:
        do_nic_status(args)

    if args.disk != SKIP_ME:
        do_disk_status(args)

    if args.fan != SKIP_ME:
        do_fan_status(args)

    if args.temp != SKIP_ME:
        do_temp_status(args)

    if args.accounts:
        do_accounts(args)

    if args.battery:
        do_battery_status(args)

    if args.bios:
        do_bios_settings(args)

    if args.reboot:
        do_reboot(args)

    if args.pcmd != SKIP_ME:
        do_power_state(args)

    if args.bmc_version:
        do_bmc_version(args)

    if args.load_firmware != SKIP_ME:
        do_load_firmware(args)

if __name__ == "__main__":
    main()
